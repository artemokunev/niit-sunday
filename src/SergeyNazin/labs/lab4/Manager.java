
/**
 * Manager
 */
public class Manager extends Employee implements Project {
    public final int budget;
    public final int amountOfColleaguesInvolved;
    public Manager (int id, String name, int budget, int amountOfColleaguesInvolved) {
        super(id, name);
        this.budget = budget;
        this.amountOfColleaguesInvolved = amountOfColleaguesInvolved;
    }
    @Override
    public void getPaid() {
        getPaidProportionallyRelativelyToImpact(this.budget, this.amountOfColleaguesInvolved);
    }
    @Override
    public void getPaidProportionallyRelativelyToImpact(int budget, int amountOfColleaguesInvolved) {
        this.payment += budget / amountOfColleaguesInvolved;
    }
}