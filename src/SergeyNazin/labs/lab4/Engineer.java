
/**
 * Engineer
 */
public abstract class Engineer extends Employee implements WorkTime, Project {
    public final int budget;
    public final int amountOfColleaguesInvolved;
    public final int base;
    public Engineer (int id, String name, int base, int worktime, int budget, int amountOfColleaguesInvolved) {
        super(id, name);
        this.base = base;
        this.worktime = worktime;
        this.budget = budget;
        this.amountOfColleaguesInvolved = amountOfColleaguesInvolved;
    }
    @Override
    public void getPaid() {
        getPaidProportionallyRelativelyToImpact(this.budget, this.amountOfColleaguesInvolved);
        multiplyHoursToBaseWageRate(this.base, this.worktime);
    }
    @Override
    public void multiplyHoursToBaseWageRate(int base, int worktime) {
        this.payment += base * worktime;
    }
    @Override
    public void getPaidProportionallyRelativelyToImpact(int budget, int amountOfColleguesInvolved) {
        this.payment += budget / amountOfColleguesInvolved;
    }
}