import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * <p><strong>
 *     Библиотека org.json.simple.* просто ужасна
 * невообразимо отвратная библиотека
 * org.json.* , (например JSONObject | JSONArray)
 * на МНОГО лучше Антон Александрович.
 * Может быть мало опыта, но все эти касты из/в Object и Эксепшены очень грязным код делают
 * </strong></p>
 * Main
 */

public class Main {

    private static final HashMap<String, Integer> BUDGET = new HashMap<String, Integer>() {{
        put("DesktopOne",    180000);
        put("EnterpriseEdition", 700000);
        put("IntegrationEdition",   980000);
    }};

    public static void main(String[] args) {
        ArrayList<Employee> allPersonell = new ArrayList<>();
        HashMap<Long, JSONObject> collection = new HashMap<>();
        HashMap<String, Integer> leaders = new HashMap<>(),
                                projects = new HashMap<>();
        try {
            File f=new File("/home/soberich/Projects/Java/niit-sunday/src/SergeyNazin/lab4/StuffDemo/src/main/java/personell.json");
            JSONParser parser=new JSONParser();
            FileReader fr = new FileReader(f);
            Object obj=parser.parse(fr);
            JSONArray employees = (JSONArray)obj;


            for(int i = 0; i < employees.size(); i++) {
                JSONObject employee = (JSONObject)employees.get(i);
                collection.put((Long) employee.get("index"), employee);
                // todo можно (нужно !) убрать if
                //if (!Heading.class.isAssignableFrom(Class.forName(employee.get("position").toString()))) {
                    for(Iterator iterator = employee.keySet().iterator(); iterator.hasNext();) {
                        String key = (String) iterator.next();
                        // check through as much cases as there are interfaces implemented by Employee children
                        switch (key) {
                            case "project":
                                if (employee.get(key).getClass().isAssignableFrom(String.class)) {
                                    if (null != projects.putIfAbsent(employee.get(key).toString(), 1)) {
                                        projects.put(employee.get(key).toString(), projects.get(employee.get(key).toString()) + 1);
                                    }
                                }
                                else {
                                    JSONArray jsonArrayProject = (JSONArray)employee.get(key);
                                    for (int k = 0; k < jsonArrayProject.size(); k++) {
                                        if (null != projects.putIfAbsent(jsonArrayProject.get(k).toString(), 1)) {
                                            projects.put(jsonArrayProject.get(k).toString(), projects.get(jsonArrayProject.get(k).toString()) + 1);
                                        }
                                    }
                                }
                                break;
                            case "boss":
                                if (employee.get(key).getClass().isAssignableFrom(String.class)) {
                                    System.out.println(employee.get(key).toString());
                                    leaders.putIfAbsent(employee.get(key).toString(), 1);
                                    leaders.put(employee.get(key).toString(), leaders.get(employee.get(key).toString()) + 1);
                                }
                                else {
                                    JSONArray jsonArrayBoss = (JSONArray) employee.get(key);
                                    for (int k = 0; k < jsonArrayBoss.size(); k++) {
                                        if (null != leaders.putIfAbsent(jsonArrayBoss.get(k).toString(), 1)) {
                                            leaders.put(jsonArrayBoss.get(k).toString(), leaders.get(jsonArrayBoss.get(k).toString()) + 1);
                                        }
                                    }
                                }
                                    break;
                            default:
                                break;
                        }
                    }
            //    }
            }

            for(int i = 0; i < employees.size(); i++) {
                JSONObject employee = (JSONObject)employees.get(i);
                HashMap<String, String> temp =  new HashMap<>(employee.size());
                for(Iterator iterator = employee.keySet().iterator(); iterator.hasNext();) {
                    String key = (String) iterator.next();
                    temp.put(key, String.valueOf(employee.get(key)));
                }

                switch(temp.get("position")) {
                    case "Driver":
                        allPersonell.add(
                                new Driver(
                                        Integer.valueOf(temp.get("index"))
                                        , temp.get("name")
                                        , Integer.valueOf(temp.get("base"))
                                )
                        );
                        break;
                    case "Cleaner":
                        allPersonell.add(
                                new Cleaner(
                                        Integer.valueOf(temp.get("index"))
                                        , temp.get("name")
                                        , Integer.valueOf(temp.get("base"))
                                )
                        );
                        break;
                    case "Manager":
                        allPersonell.add(
                                new Manager(
                                        Integer.valueOf(temp.get("index"))
                                        , temp.get("name")
                                        , BUDGET.get(temp.get("project"))
                                        , projects.get(temp.get("project"))
                                )
                        );
                        break;
                    case "Programmer":
                        allPersonell.add(
                                new Programmer(
                                        Integer.valueOf(temp.get("index"))
                                        , temp.get("name")
                                        , Integer.valueOf(temp.get("base"))
                                        , Integer.valueOf(temp.get("worktime"))
                                        , BUDGET.get(temp.get("project"))
                                        , projects.get(temp.get("project"))
                                )
                        );
                        break;
                    case "ProjectManager":
                        int numSubordinates = leaders.get(temp.get("name"));
                        allPersonell.add(
                                new ProjectManager(
                                        Integer.valueOf(temp.get("index"))
                                        , temp.get("name")
                                        , BUDGET.get(temp.get("project"))
                                        , projects.get(temp.get("project"))
                                        , numSubordinates
                                )
                        );
                        break;
                    case "SeniorManager":
                        int numSubSubordinates = leaders.get(temp.get("name"));
                        allPersonell.add(
                                new SeniorManager(
                                        Integer.valueOf(temp.get("index"))
                                        , temp.get("name")
                                        , BUDGET.get(temp.get("project"))
                                        , projects.get(temp.get("project"))
                                        , numSubSubordinates
                                )
                        );
                        break;
                    case "TeamLeader":
                        int numProgrammerTeamSubordinates = leaders.get(temp.get("name"));
                        allPersonell.add(
                                new TeamLeader(
                                        Integer.valueOf(temp.get("index"))
                                        , temp.get("name")
                                        , Integer.valueOf(temp.get("base"))
                                        , Integer.valueOf(temp.get("worktime"))
                                        , BUDGET.get(temp.get("project"))
                                        , projects.get(temp.get("project"))
                                        , numProgrammerTeamSubordinates
                                )
                        );
                        break;
                    case "Tester":
                        allPersonell.add(
                                new Tester(
                                        Integer.valueOf(temp.get("index"))
                                        , temp.get("name")
                                        , Integer.valueOf(temp.get("base"))
                                        , Integer.valueOf(temp.get("worktime"))
                                        , BUDGET.get(temp.get("project"))
                                        , projects.get(temp.get("project"))
                                )
                        );
                        break;
                    default:
                        System.out.println("SomethingWrong!");
                        break;
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        } /*catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }*/
        for (Employee employee : allPersonell) {
            employee.getPaid();
            System.out.println("===================================================================");
            System.out.println(employee.getClass());
            System.out.println("id       ---> "+employee.id);
            System.out.println("name     ---> "+employee.name);
            System.out.println("worktime ---> "+employee.worktime);
            System.out.println("payment  ---> "+employee.payment);
            System.out.println("\n+++++++++++++++ original JSON looked like (for check) +++++++++++++\n");
            System.out.println(collection.get(Long.valueOf(employee.id)).toString());
            System.out.println("===================================================================");

        }
        System.out.println("\n######################### All BOSSES and their subordinates number ! #####################\n");
        System.out.println(leaders);
        System.out.println("\n######################### All PROJECTS and involved number ! #####################\n");
        System.out.println(projects);
    }
}