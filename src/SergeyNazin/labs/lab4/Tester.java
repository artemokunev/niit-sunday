

/**
 * Tester
 */
public class Tester extends Engineer {

    public Tester (int id, String name, int base, int worktime, int budget, int amountOfColleaguesInvolved) {
        super(id, name, base, worktime, budget, amountOfColleaguesInvolved);
    }
}