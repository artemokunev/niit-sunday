

/**
 * Project
 */
public interface Project {
    void getPaidProportionallyRelativelyToImpact(int budget, int amountOfColleguesInvolved);
}