

/**
 * Heading
 */
public interface Heading {
    int CASH_FOR_SUBORDINATE = 500;
    void getPaidForSubordinates(int numSubordinates);
}