import java.util.concurrent.ExecutionException;

/**
 * yeah
 * Created by soberich on 09.07.2017.
 */
public class Automata {
    public enum STATES { OFF, WAIT, ACCEPT, CHECK, COOK }; // TODO WHY "FINAL" SPECIFIER IS NOT ALLOWED ???
    private int cash = 0;
    private STATES state = STATES.OFF;
    private final int prices[] = { 60, 40, 12, 5 , 100};
    private final String menu[] = { "Coffee", "Cacao", "Soup", "Boiled water", "Vodka" };

    private void throwWrongState() throws Exception {
        throw new Exception(new StringBuffer("EXCEPTION!!! Automata object is in unappropriate state: \"")
                                    .append(state.toString())
                                    .append("\" for to call it from method Automata::")
                                    .append(Thread.currentThread().getStackTrace()[2].getMethodName())
                                    .append( "()").toString());
        
    }
    public Automata() {
    }
    public String on() throws Exception {
        if (STATES.OFF == state) {
            state = STATES.WAIT;
            return "Hello!";
        }
        else throwWrongState();
        return null;
    }
    public String off() throws Exception {
        if (STATES.WAIT == state) {
            state = STATES.OFF;
            return "Good bye!";
        }
        else throwWrongState();
        return null;
    }
    public String coin(int __cash) throws Exception {
        if (STATES.WAIT == state || STATES.ACCEPT == state) {
            state = STATES.ACCEPT;
            cash += __cash;
            return (new StringBuffer("Your balance is ").append(cash).append("\n")).toString();
        }else throwWrongState();
        return null;
    }

    /*private void changeState(STATES st, int cashDelta = 0) {
        switch (st) {
            case ACCEPT:
                if (STATES.WAIT == state || STATES.ACCEPT == state) {
                    state = STATES.ACCEPT;
                    cash += cashDelta;
                }
                break;

        }
    }*/

    public String printMenu() {
        StringBuffer tempStr = new StringBuffer();
        for (String var : menu) {
            tempStr.append(var).append("\n");
        }
        return tempStr.toString();
    }
    public String printState() {
        return state.toString();
    }
    public String choice(String s) throws Exception {
        if (STATES.ACCEPT == state) {
            state = STATES.CHECK;
            for(int i = 0; i < menu.length; ++i) {
                if(s.equals(menu[i]))
                    return check(i);
            }
            throw new Exception("No such thing in menu");
        } else throwWrongState();
        return null;
    }

    private String check(int i) throws Exception {
        if (cash >= prices[i])
            return cook(i);
        return cancel();
         
    }
    public String cancel() throws Exception {
        int temp = cash;
        if (STATES.ACCEPT == state || STATES.CHECK == state) {
            state = STATES.WAIT;
            cash = 0;
            return (new StringBuffer("Operation canceled by user or balance is inconsistent. Your change is ")                   .append(temp)
                            .append(" USD!\n")).toString();
        }else throwWrongState();
        return null;
    }
    private String cook(int i) throws Exception {
        state = STATES.COOK;
        return finish(i);
        
    }
    private String finish(int i) throws Exception {
        if (STATES.COOK == state) {
            state = STATES.WAIT;
            StringBuffer temp = new StringBuffer();
            cash -= prices[i];
            if (cash > 0)
                temp.append("Your change is ").append(cash).append("\n");
            cash = 0;
            return (new StringBuffer("You picked ")
                            .append(menu[i])
                            .append(" for ")
                            .append(prices[i])
                            .append(" USD")
                            .append(temp.toString())
                            .append("\n")
                            .append("Operation successfully finished. Have a nice drink!\n")).toString();
        }else throwWrongState();
        return null;
    }
}
