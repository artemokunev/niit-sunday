public class Main {
    // static { /// This DOES NOT WORK. WHY ?????
    //     final int MONEY_IN = 100;
    //     final String DRINK = "Vodka";
    // }
    private static final int MONEY_IN = 100;
    private static final String DRINK = "Vodka";
    public static void main(String[] args) {
        StringBuffer buffer = new StringBuffer();
        Automata A = new Automata();
        try {
            A.on();

            buffer.append("Opereation starts\n");
            buffer.append("Please put some money in....\n");

            buffer.append(A.printState()).append("\n");

            buffer.append("You have put ").append(MONEY_IN).append(" USD more\n");
            buffer.append(A.coin(MONEY_IN));

            buffer.append("==MENU==\n");
            buffer.append(A.printMenu());

            buffer.append("Current state is\n");
            buffer.append(A.printState()).append("\n");

            //A.off();

            buffer.append(A.choice(DRINK));

            buffer.append("New state is\n");
            buffer.append(A.printState()).append("\n");

            A.off();

            //A.cancel();

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(buffer);

    }
}
