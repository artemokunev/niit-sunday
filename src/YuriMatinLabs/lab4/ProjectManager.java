/**
 * Проект-менеджер, осуществляет управление конкретным проектом
 */
public class ProjectManager extends Manager implements Heading {

    int numOfSubordinates;

    public ProjectManager(int ID, String name,
                          String projectName,
                          int projectBudget,
                          double participleInProject,
                          int numOfSubordinates) {
        super(ID, name, projectName, projectBudget, participleInProject);
        this.numOfSubordinates=numOfSubordinates;
        this.position = "проект-менеджер";
    }

    @Override
    void calcPayment() {
       this.payment= getProjectBonus()+getHeadingBonus();
    }

    @Override
    public int getHeadingBonus() {
        return numOfSubordinates*700;
    }

    @Override
    public String toString() {
        return super.toString();
    }




}
