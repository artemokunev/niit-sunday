/**
 * Инженер, родительский класс для работников, учавствующих в проекте;
 */
public abstract class Engineer extends Employee implements WorkTime, Project {
    //Инженер имеет как поле "базовая ставка", так и новые: "название проекта",
    // "ежемесячный бюджет пректа" и "вклад в проект"
    int BASE;
    String projectName;
    int projectBudget;
    double participleInProject;


    public Engineer(int ID, String name, int workTme, int BASE, String projectName,
                    int projectBudget, double participleInProject) {
        super(ID, name);
        this.workTme=workTme;
        this.BASE=BASE;
        this.projectName=projectName;
        this.projectBudget=projectBudget;
        this.participleInProject=participleInProject;
    }

    void calcPayment() {
        this.payment = getPaymentForWorkTime()+getProjectBonus();
    }

    @Override
    public int getPaymentForWorkTime() {
        return BASE * workTme;
    }

    @Override
    public double getProjectBonus() {
        return projectBudget/100*participleInProject;
    }

    @Override
    public String toString() {
        return "Сотрудник "+getName()+" ID: "+getID()+"; занимаемая должность: "
                +position+"; учавствует в проекте: "+projectName+"; отработанное время: "+Integer.toString(workTme)
                +" часов"+"; начисленная зарплата: "+Double.toString(getPayment())+" рублей" ;
    }
}
