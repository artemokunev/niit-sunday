/**
 * Менеджер, занимается разными организационными вопросами
 */
public class Manager extends Employee implements Project {
    String projectName;
    double projectBudget;
    double participleInProject;


    public Manager(int ID, String name,
                   String projectName,
                   int projectBudget,
                   double participleInProject) {
        super(ID, name);
        this.projectName=projectName;
        this.projectBudget=projectBudget;
        this.participleInProject=participleInProject;
        this.position = "менеджер";
    }

    @Override
    void calcPayment() {
        this.payment = getProjectBonus()+2000;
    }

    @Override
    public double getProjectBonus() {
        return projectBudget/100*participleInProject;
    }

    @Override
    public String toString() {
        return "Сотрудник "+getName()+" ID: "+getID()+"; занимаемая должность: "
                +position+"; учавствует в проекте: "+projectName+"; начисленная зарплата: "+Double.toString(getPayment())+" рублей" ;
    }
}
