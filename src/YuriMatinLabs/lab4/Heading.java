/**
 * Расчет оплаты исходя из руководства (количество подчиненных).
 */
public interface Heading {
    //Оплата за руководство
    int getHeadingBonus();
}
