/**
 * Тестер
 */
public class Tester extends Engineer{

    public Tester(int ID, String name, int workTme, int BASE,
                  String projectName,
                  int projectBudget,
                  double participleInProject) {
        super(ID, name, workTme, BASE, projectName, projectBudget, participleInProject);
        this.position = "тестировщик";
    }

}
