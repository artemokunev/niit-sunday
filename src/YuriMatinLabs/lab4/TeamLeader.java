/**
 * Тимлид, старший программист, ведущий программист,
 * руководитель программистов; получает конкретную задачу по проекту от
 * проект-менеджера и уже со своими подчиненными ее решает
 */
public class TeamLeader extends Programmer implements Heading {

    //Поле количество подчиненных
    int nunOfSubordinates;

    public TeamLeader(int ID, String name, int workTme, int BASE,
                      String projectName,
                      int projectBudget,
                      double participleInProject,
                      int nunOfSubordinates) {
        super(ID, name, workTme, BASE,projectName, projectBudget, participleInProject);
        this.nunOfSubordinates=nunOfSubordinates;
        this.position = "тимлидер";
    }


    @Override
    void calcPayment() {
        this.payment = getPaymentForWorkTime() + getProjectBonus() + getHeadingBonus();
    }

    @Override
    public int getHeadingBonus() {
        return nunOfSubordinates*100;
    }
}
