
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.*;
import java.util.Date;
import java.util.Random;


/**
 * Измененная версия многопоточного сервера со страницы http://shtanyuk.tk
 */

class ServerOne extends Thread {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    public ServerOne(Socket s) throws IOException {
        socket = s;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        start();
    }

    //Задание №1, сервер высылает значение даты и времени
    public String getDateAndTime() {
        Date date = new Date();
        String s = String.format("%te %<tB %<tY %<tT", date);
        return s;
    }

    //Задание №2, сервер высылает афоризм из файла
    public String getAphorism() {
        String randAphorism;
        try {
            Random rand = new Random();
            FileReader file = new FileReader("Aphorism.json");
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) (parser.parse(file));
            JSONArray aphorismArray = (JSONArray) jsonObject.get("aphorism");
            randAphorism = (((JSONObject) aphorismArray.get(
                    rand.nextInt(aphorismArray.size()))).get("content")).toString();

        } catch (ParseException | IOException e) {
            return randAphorism = "Мудрость закончилась!";
        }
        return randAphorism;
    }

    /**
     * Задание №3; реализация: при нажатии в окне клиента кнопки "Запланированные события"
     * на сервер отправляется имя клиента, которое данный метод принимает и проверяет на
     * совпадение с хранящимися в файле именами, которым соответствуют события
     */
    public String getEvent(String name) {
        if (name==null)return "имя не указано!";
        String stringToSend = "";
        try {
            FileReader file = new FileReader("Events.json");
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) (parser.parse(file));
            JSONArray clientsArray = (JSONArray) jsonObject.get("client");
            for (Object client : clientsArray) {
                String clientName = (((JSONObject) client).get("clientID")).toString();
                if (name.equals(clientName)) {
                    JSONArray eventsArray = (JSONArray) (((JSONObject) client).get("events"));
                    String[] eventsStringArr = new String[eventsArray.size()];
                    for (int i = 0; i < eventsArray.size(); i++) {
                        String time = (((JSONObject) eventsArray.get(i)).get("time")).toString();
                        String description = (((JSONObject) eventsArray.get(i)).get("description")).toString();
                        eventsStringArr[i] = time + " >>>> " + description;
                        stringToSend += eventsStringArr[i] + "new";
                    }
                    return stringToSend;
                }

            }


        } catch (ParseException | IOException e) {
            return "хм, что-то не так...";
        }
        return stringToSend = "Для клиента " + name + " записи о запланированных событиях отсутствуют";

    }


    public void run() {
        try {
            while (true) {
                String str = in.readLine();
                char[] strToChars = str.toCharArray();
                if (strToChars[0]=='3') out.println(getEvent(str.substring(1,str.length())));

                if (str.equals("END")) break;

                switch (str) {
                    case "1":
                        out.println(getDateAndTime());
                        break;
                    case "2":
                        out.println(getAphorism());
                        break;
                    case "4":
                        out.println("Пока не реализовано...");
                        break;
                }
            }
        } catch (IOException e) {

        } finally {
            try {
                out.println("Сервер недоступен!");
                socket.close();
            } catch (IOException e) {
                infoMessage("ok", "Сокет не закрыт");
            }
        }
    }

    //Метод создания информирующего сообщения
    private void infoMessage(String btnName, String infoMess) {
        JFrame frame = new JFrame();
        frame.setSize(400, 200);
        frame.setLocationRelativeTo(null);
        frame.add(new JLabel(infoMess, 0), BorderLayout.CENTER);
        JPanel panel = new JPanel();
        JButton button = new JButton(btnName);
        button.addActionListener(e -> frame.setVisible(false));
        panel.add(button);
        frame.add(panel, BorderLayout.SOUTH);
        frame.setVisible(true);
    }
}


class Server {
    private int port;

    public Server(int port) throws IOException {
        this.port = port;
        ServerSocket s = new ServerSocket(port);
        JFrame serverWindow = new JFrame("Server");
        serverWindow.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                try {
                    s.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        });
        serverWindow.setBounds(300, 300, 300, 300);
        serverWindow.setLayout(new BorderLayout());
        JLabel info = new JLabel("Сервер запущен...вроде", 0);
        serverWindow.add(info, BorderLayout.CENTER);
        serverWindow.setVisible(true);
        Socket socket = null;
        try {
            while (true) {
                socket = s.accept();
                try {
                    new ServerOne(socket);
                } catch (IOException e) {
                    socket.close();
                }
            }
        } catch (SocketException e) {
            assert socket != null;
            socket.close();
        } finally {
            s.close();
        }


    }


}



