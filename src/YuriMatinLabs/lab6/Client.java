import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;


/**
 * Клиент, реализован в виде отдельного окна с кнопками,
 * каждая из которых является реализацией одного из четырех
 * заданий лабораторной №6
 */
class Client {
    private String name;
    private Socket server;
    private BufferedReader fromServer;
    private PrintWriter toServer;

    public String getName() {
        return name;
    }

    Client(int port, String name) throws IOException {
        this.name = name;
        server = new Socket(InetAddress.getLocalHost(), port);
        fromServer = new BufferedReader(new InputStreamReader(server.getInputStream()));
        toServer = new PrintWriter(server.getOutputStream(), true);
        Stage cStage = new Stage();
        BorderPane pane = new BorderPane();
        VBox cRoot = new VBox();

        Label messageWindow = new Label();
        messageWindow.setWrapText(true);
        messageWindow.setTextAlignment(TextAlignment.CENTER);


        cStage.setOnCloseRequest(event -> {
            try {
                toServer.println("END");
                server.close();
            } catch (IOException e) {
                messageWindow.setText("Не получается!");
            }
        });

        Button closeButton = new Button("Закрыть соединение");
        closeButton.setOnAction(event -> {
            toServer.println("END");
            messageWindow.setText("Соединение закрыто");
            try {
                server.close();
            } catch (IOException e) {
                messageWindow.setText("Не получается!");
            }
        });

        //Первое задание
        Button getDateAndTime = new Button("Дата и время");
        getDateAndTime.setOnAction(event -> {
            toServer.println("1");
            try {
                messageWindow.setText(fromServer.readLine());
            } catch (SocketException e){
                messageWindow.setText("Соединение прервано");
            } catch (IOException e) {
                messageWindow.setText("Что-то случилось!");
            }
        });

        //Второе задание
        Button getAphorism = new Button("Надели меня мудростью, машина!");
        getAphorism.setOnAction(event -> {
            toServer.println("2");
            try {
                messageWindow.setText(fromServer.readLine());
            }  catch (SocketException e){
                messageWindow.setText("Соединение прервано");
            } catch (IOException e) {
                messageWindow.setText("Что-то случилось!");
            }
        });

        //Третье задание
        Button getEventMessage = new Button("Запланированные события");
        getEventMessage.setOnAction(event -> {
            toServer.println("3"+getName());
            try {
                String recievedString = fromServer.readLine();
                if (recievedString.contains("new")){
                    messageWindow.setTextAlignment(TextAlignment.LEFT);
                    messageWindow.setText( recievedString.replace("new","\n"));
                }else{
                    messageWindow.setText(recievedString);
                }
            } catch (SocketException e){
                messageWindow.setText("Соединение прервано");
            } catch (IOException e) {
                messageWindow.setText("Что-то случилось!");
            }
        });

        //Четвертое задание
        Button converter = new Button("Запустить конвертер валют");
        converter.setOnAction(event -> {
            toServer.println("4");
            try {
                messageWindow.setText(fromServer.readLine());
            } catch (SocketException e){
                messageWindow.setText("Соединение прервано");
            } catch (IOException e) {
                messageWindow.setText("Что-то случилось!");
            }
        });

        cRoot.getChildren().addAll(getDateAndTime, getAphorism, getEventMessage, converter);
        pane.setTop(cRoot);
        pane.setCenter(messageWindow);
        pane.setBottom(closeButton);
        Scene serverWindow = new Scene(pane, 500, 500);
        cStage.setScene(serverWindow);
        cStage.show();
        cStage.setOnCloseRequest(event -> toServer.println("END"));

    }


}
