/**
 * Automata графическая версия
 */

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Automata {
    private STATES state ;
    //внесенные наличные
    private double cash = 0;
    //требуемое количество денег(цена напитка)
    private double needMoney;
    //выбранный напиток
    private String yourDrinkName = "";
    //Контейнер напитков
    ArrayList<Drink> drinks = new ArrayList<>();

    enum STATES {
        on(" Включен!... "),
        off(" Выключен! Включите автомат... "),
        waitingChoice(" Ожидание выбора... "),
        cooking(" Приготовление напитка... "),
        giveOddMoney(" Выдаю деньги... "),
        giveDrink(" Выдаю напиток... "),
        cashNow(" наличные на текущий момент...");
        final String string;

        STATES(String string) {
            this.string = string;
        }


        public String PrintState() {
            return string;
        }

    }

    //Класс "Напиток"
    class Drink {
        private String name;
        private double price;

        public Drink(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }
    }

    //Вспомогательные методы
    double getCash() {
        return cash;
    }

    void setCash(double cash) {
        this.cash = cash;
    }

    double getNeedMoney() {
        return needMoney;
    }

     void setNeedMoney(double needMoney) {
        this.needMoney = needMoney;
    }

    String getYourDrinkName() {
        return yourDrinkName;
    }

    void setYourDrinkName(String yourDrinkName) {
        this.yourDrinkName = yourDrinkName;
    }


    //Основные методы
    public void On()  {
        this.state = STATES.on;
        state.PrintState();
    }

    public void Off() {
        this.state = STATES.off;
        state.PrintState();
    }

    //Заполняю лист напитков
    private void addDrinksFromFile(String filename) {
        try {
            FileReader file = new FileReader(filename);
            JSONParser parser = new JSONParser();
            Object parseObject = parser.parse(file);
            JSONObject jsObject = (JSONObject) parseObject;
            JSONArray drinksArray = (JSONArray) jsObject.get("drinks");
            for (Object i : drinksArray) {
                Drink drink = new Drink(
                        (((JSONObject) i).get("name")).toString(),
                        Double.parseDouble((((JSONObject) i).get("price")).toString()));

                drinks.add(drink);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void buildMenu(){
        this.state = STATES.waitingChoice;
        state.PrintState();
        if (drinks.size()==0) System.out.println("Напитки не обнаружены");
        else {
            String drinksChoise = "Для выбора напитка нажмите ";
            for (int i = 0; i < drinks.size(); i++) {
                System.out.println((i + 1) + ")" + drinks.get(i).getName() + " стоимость: " + drinks.get(i).getPrice() + " руб.");
                drinksChoise += i + 1 + ", ";
            }
            System.out.println(drinksChoise);
        }
    }

    public void addCoins(int coin) {
        this.cash += coin;
    }

    public boolean checkCash() {
        this.state = STATES.cashNow;
        state.PrintState();
        if (cash >= needMoney) return true;
        else return false;
    }

    public void chooseDrink(int drinkNumber) {
        if(drinkNumber>drinks.size()) System.out.println("напиток с таким номером не обнаружен");
        else {
            yourDrinkName = drinks.get(drinkNumber-1).getName();
            setNeedMoney(drinks.get(drinkNumber-1).getPrice());
        }
    }

    public void Cook() {
        this.state = STATES.cooking;
        state.PrintState();
        if(yourDrinkName.equals("")) System.out.println("Напиток не выбран");
        else if (!checkCash()) System.out.println(
                "Недостаточно наличных, необходими еще "+ Double.toString(getNeedMoney()-getCash())+" руб.");
        else {
            cash -= needMoney;
            for (int i = 0; i < 4; i++) {
                System.out.println("Cook!");
            }
            takeDrink();
        }
    }

    public void takeDrink() {
        this.state = STATES.giveDrink;
        state.PrintState();
         System.out.println("Ваш напиток "+ getYourDrinkName());
    }

    public void returnMoney() {
        this.state = STATES.giveOddMoney;
        state.PrintState();
        System.out.println(String.format("Заберите деньги: " + "%.2f%s", getCash(), " руб."));
        setCash(0);

    }



    //    Конструктор Automata
    public Automata(String drinksFileName) {
        addDrinksFromFile(drinksFileName);
    }


}
