import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 *
 */
public class AutomataWindow extends Automata{
    private JButton chDrink, bringCash;
    private JPanel bringMoneyPanel;
    private JPanel drinksMenu;
    private JPanel manageButtons;

    @Override
    public void Cook() {
        new Thread(() -> {
            JFrame fr = new JFrame("Cooking!!!");
            fr.setSize(400, 300);
            fr.setLocationRelativeTo(null);
            fr.setLayout(new BorderLayout(5, 5));
            fr.setVisible(true);

            JLabel info = new JLabel("", 0);
            fr.add(info);

            JButton getYourDrink = new JButton("Забрать напиток");
            getYourDrink.addActionListener(e -> {
                takeDrink();
                fr.setVisible(false);
            });
            JPanel panel = new JPanel();
            panel.add(getYourDrink);
            panel.setVisible(false);

            fr.add(panel, BorderLayout.SOUTH);

            int timing = 0;
            String message = ">";
            while (timing != 8) {
                info.setText("Напиток готовится" + message);
                message += ">";
                timing++;
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            info.setText("Напиток готов!");
            panel.setVisible(true);

        }).start();
    }

    @Override
    public void takeDrink() {
        JFrame drinkFrame = new JFrame();
        drinkFrame.setSize(300, 300);
        drinkFrame.setLayout(new BorderLayout(10,10));
        drinkFrame.setLocationRelativeTo(null);
        drinkFrame.setVisible(true);
        drinkFrame.add(new JPanel().add(
                new JLabel("<html>Ваш напиток<br><br>"+getYourDrinkName()+"</html>", 0)), BorderLayout.CENTER);
        JPanel panel = new JPanel();
        JButton button = new JButton("ОК");
        button.addActionListener(e -> drinkFrame.setVisible(false));
        panel.add(button);
        drinkFrame.add(panel, BorderLayout.SOUTH);
    }

    @Override
    public void returnMoney() {
        JFrame moneyFrame = new JFrame();
        moneyFrame.setSize(300, 300);
        moneyFrame.setLayout(new BorderLayout(10,10));
        moneyFrame.setLocationRelativeTo(null);
        moneyFrame.setVisible(true);
        moneyFrame.add(new JPanel().add(
                new JLabel("<html>Ваши наличные<br><br>"+doubleMetamorf(getCash())+"</html>", 0)), BorderLayout.CENTER);
        JPanel panel = new JPanel();
        JButton button = new JButton("ОК");
        button.addActionListener(e -> moneyFrame.setVisible(false));
        panel.add(button);
        moneyFrame.add(panel, BorderLayout.SOUTH);
        setCash(0);
        bringCash.setText("Внесенные наличные: " + doubleMetamorf(getCash()));
    }


    public AutomataWindow(String drinksFileName) {
        super(drinksFileName);

        JFrame mainWindow = new JFrame("Automata");
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setBounds(500, 300,800, 600);

        mainWindow.setLayout(new BorderLayout(5, 5));

        //Панель вывода информации
        chDrink = new JButton("Напиток не выбран");
        bringCash = new JButton("Внесенные наличные: 0 рублей");
        chDrink.setEnabled(false);
        bringCash.setEnabled(false);
        JPanel infoPanel = new JPanel();
        infoPanel.setVisible(true);
        infoPanel.setLayout(new GridLayout(2, 1));
        infoPanel.add(chDrink);
        infoPanel.add(bringCash);
        mainWindow.add(infoPanel, BorderLayout.NORTH);

        //Панель внесения наличных
        bringMoneyPanel = new JPanel();
        bringMoneyPanel.setVisible(true);
        bringMoneyPanel.setLayout(new GridLayout(6, 1, 10, 10));
        bringMoneyPanel.add(new JLabel("Внесение наличных", 0));
        addBringMoneyButton("50 рублей", 50);
        addBringMoneyButton("10 рублей", 10);
        addBringMoneyButton("5 рублей", 5);
        addBringMoneyButton("2 рубля", 2);
        addBringMoneyButton("1 рубль", 1);
        mainWindow.add(bringMoneyPanel, BorderLayout.WEST);

        //Меню напитков
        drinksMenu = new JPanel();
        drinksMenu.setVisible(true);
        int drinksSize = drinks.size();
        if (drinksSize >= 5 && drinksSize % 5 == 0)
            drinksMenu.setLayout(new GridLayout(5, drinksSize / 5, 10, 10));
        else if (drinksSize >= 5 && drinksSize % 5 != 0)
            drinksMenu.setLayout(new GridLayout(5, drinksSize / 5 + 1, 10, 10));
        else drinksMenu.setLayout(new GridLayout(drinksSize, 1, 10, 10));

        for (Drink drink : drinks) {
            createDrinkButton(drink.getName(), drink.getPrice());
        }
        mainWindow.add(drinksMenu, BorderLayout.EAST);

        //Панель управления
        manageButtons = new JPanel();
        manageButtons.setVisible(true);
        manageButtons.setLayout(new GridLayout(2, 1, 10, 10));
        JButton cookButton = new JButton("Приготовить");
        JButton returnCash = new JButton("Получить сдачу");

        cookButton.addActionListener(event -> {
            if (getYourDrinkName().equals("")) infoMessage(
                    "OK", "Напиток не выбран");
            else if (!checkCash()) infoMessage(
                    "ОК", "Недостаточно наличных! Необходимо еще " +
                            doubleMetamorf(getNeedMoney() - getCash()));
            else {

                setCash((int) (getCash() - getNeedMoney()));
                bringCash.setText("Наличные к выдаче: " + doubleMetamorf(getCash()));
                Cook();
            }
        });
        returnCash.addActionListener(event-> returnMoney());

        manageButtons.add(cookButton);
        manageButtons.add(returnCash);
        mainWindow.add(manageButtons);
        mainWindow.setVisible(true);
    }


    //Метод создания информирующего сообщения
    private void infoMessage(String btnName, String infoMess) {
        JFrame frame = new JFrame();
        frame.setSize(400, 200);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.add(new JLabel(infoMess, 0), BorderLayout.CENTER);
        JPanel panel = new JPanel();
        JButton button = new JButton(btnName);
        button.addActionListener(e -> frame.setVisible(false));
        panel.add(button);
        frame.add(panel, BorderLayout.SOUTH);
    }


    //Метод создания кнопок выбора напитков
    private void createDrinkButton(String name, double price) {
        JButton button = new JButton(
                "<html>"+name+"<br>" + " цена: " + doubleMetamorf(price)+"</html>");
        drinksMenu.add(button);
        button.addActionListener(event -> {
                    setYourDrinkName(name);
                    setNeedMoney(price);
                    chDrink.setText("Выбран напиток " + "<<" + name + ">>");
                }
        );
    }

    //Метод создания кнопок внесения наличных
    private void addBringMoneyButton(String name, int money) {
        JButton button = new JButton(name);
        bringMoneyPanel.add(button);
        button.addActionListener(event -> {
                    addCoins(money);
                    bringCash.setText("Внесенные наличные: " + doubleMetamorf(getCash()));
                }
        );


    }

    //Метод, преобразующий число с плавающей точкой в строку "Х рублей У копеек"
    private String doubleMetamorf(double number) {
        int intValue = (int) number;
        int fractValue = (int)((number-intValue)*100);

        return Integer.toString(intValue) + "руб. " + Integer.toString(fractValue) + "коп.";
    }

}
