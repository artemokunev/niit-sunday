/**
 *
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.time.LocalTime;

public class AnalogClock extends JComponent implements Runnable
{
    public AnalogClock()  {
        (new Thread(this)).start();
    }

    public void run() {
        try {
            while (true){
                Thread.sleep(1);
                repaint();
            }
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    public void paint(Graphics graphics) {
        super.paint(graphics);

        LocalTime lt=LocalTime.now();

        long minute=lt.getMinute();
        long sec = lt.getSecond();
        long hour = lt.getHour();
        long t = System.currentTimeMillis();

        double sec_angle = t*0.006*Math.PI/180;
        double min_angle = 6.0*Math.PI/180*(minute+sec/60.0);
        double hour_angle = 30.0*Math.PI/180*(hour+minute/60.0);

        // рисую часы
        Graphics2D g = (Graphics2D) graphics;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        size = getSize(size);

        int radius = Math.min(size.width,size.height) / 2;

        g.translate((double)size.width/2D,(double)size.height/2D);

        g.setColor(Color.black);

        //Рисую циферблат
        Image background = new ImageIcon("циферблат.png").getImage();
        g.drawImage(background,-radius,-radius, radius*2,radius*2,null);

        // минутная стрелка
        Image minImg = new ImageIcon("min_angle.png").getImage();
        g.rotate(min_angle);
        g.drawImage(minImg,-radius,-radius, radius*2,radius*2,null);
        g.rotate(-min_angle);

        // стрелка часов
        Image hourImg = new ImageIcon("hour_angle.png").getImage();
        g.rotate(hour_angle);
        g.drawImage(hourImg,-radius,-radius, radius*2,radius*2,null);
        g.rotate(-hour_angle);

        // секундная стрелка
        Image secImg = new ImageIcon("sec_angle.png").getImage();
        g.rotate(sec_angle);
        g.drawImage(secImg,-radius,-radius, radius*2,radius*2,null);
        g.rotate(-sec_angle);

    }
    private Dimension size = null;

    public static void main(String[] args)
    {
        JFrame f = new JFrame("Analog Clock");
        AnalogClock clock = new AnalogClock();
        f.setSize(500,500);
        f.setLocationRelativeTo(null);

        f.add(clock);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        f.setVisible(true);
    }

}