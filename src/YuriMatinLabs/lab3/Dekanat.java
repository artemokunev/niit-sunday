package lab3;

import java.io.*;
import java.util.ArrayList;


/**
 * Лабораторная №3
 */
public class Dekanat {

//    String pathOfStudentsFile = "Students.txt";
//    String pathOfGroupsFile = "Groups.txt";

    ArrayList<Student> studentsList = new ArrayList<>();
    ArrayList<Group> groupsList = new ArrayList<>();

    //В массив prepareToFire будут добавлены студенты к отчислению
    ArrayList<Student> prepareToFire = new ArrayList<>();


    //Конструктор
    public Dekanat() {
    }

    //Добавление нового студента
    public Student createNewStudent(String name, int ID) {
        Student student = new Student(name, ID);
        studentsList.add(student);
        return student;
    }

    //Создание студентов на основе данных из файла
    public void createStudentByFile(String filename) throws IOException {
        try {
            // ArrayList<Student> ArrStudent = new ArrayList<>();
            FileReader readStudents = new FileReader(filename);
            BufferedReader bufferedStudents = new BufferedReader(readStudents);
            String filestring = bufferedStudents.readLine();
            while (filestring != null) {
                String[] studentsData = filestring.split("_");
                Student student = new Student(studentsData[3], Integer.valueOf(studentsData[0]));
                student.groupID = Integer.parseInt(studentsData[1]);
                student.Marks = studentsData[4];
                if (studentsData[2].equals("1")) student.head = true;
                studentsList.add(student);
                filestring = bufferedStudents.readLine();
            }
            bufferedStudents.close();
            // studentsList = ArrStudent;
        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        }


    }

    //Создание групп на основе данных из файла
    public void createGroupsByFile(String filename) throws IOException {
        try {
            FileReader readGroups = new FileReader(filename);
            BufferedReader bufferedGroups = new BufferedReader(readGroups);
            String filestring = bufferedGroups.readLine();
            while (filestring != null) {
                String[] groupsData = filestring.split("_");
                Group group = new Group(groupsData[0], Integer.parseInt(groupsData[1]));
                if (groupsData[2].equals("000000")) group.headID = 0;
                if (groupsData.length > 3) {
                    String[] studentsRefs = groupsData[3].split(",");
                    for (String elem : studentsRefs) {
                        int elemToInt = Integer.parseInt(elem);
                        for (int i = 0; i < studentsList.size(); i++) {
                            if (elemToInt == studentsList.get(i).getID()) {
                                group.addStudent(studentsList.get(i));
                                if (studentsList.get(i).head && studentsList.get(i).getID() == Integer.parseInt(groupsData[2])) {
                                    group.headAppointment(studentsList.get(i));
                                }
                            }
                        }
                    }
                }
                groupsList.add(group);
                filestring = bufferedGroups.readLine();
            }
            bufferedGroups.close();
        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        }
    }

    //Добавление случайных оценок студентам
    public void addRandomMark(Student student) {
        if (student.numberOfMarks() < 5) student.addMark(3 + (int) (Math.random() * 3));
        else System.out.println("Студент уже получил все оценки");
    }

    //Накопление статистики по успеваемости
     /* С указанием кандидатов на отчисление.
     Условием отчисления считаю отсутствие трех оценок при среднем балле 3*/
    public void statistics() {
        String studentsToFire = " ";
        for (int i = 0; i < groupsList.size(); i++) {
            double averageMark = groupsList.get(i).calcAverageMarkInGroup();
            int amountOfMarks = groupsList.get(i).calcAmountOfMarksInGroup();
            int maxAmountOfMarks = groupsList.get(i).calcMaxAmountOfMarksInGroup();
            if (amountOfMarks != maxAmountOfMarks) {
                for (int j = 0; j < groupsList.get(i).studentsReferences.size(); j++) {
                    Student thisStudent = groupsList.get(i).studentsReferences.get(j);
                    if (thisStudent.numberOfMarks() < 3 && thisStudent.calcAverageMark() <= 3) {
                        prepareToFire.add(thisStudent);
                        studentsToFire += thisStudent.getFIO() + ", ";
                    }
                }
            }
            System.out.println("Статистика по группе " + "<< " + groupsList.get(i).getGroupNAME() + " >>");
            System.out.printf("Средний балл в группе: " + "%.1f", averageMark);
            System.out.printf(" " + "при количестве оценок " + amountOfMarks + " из " + maxAmountOfMarks + " возможных");
            System.out.println("\n" + "Статистика по студентам ");
            for (Student person : groupsList.get(i).studentsReferences) {
                if (person.head) {
                    System.out.printf(person.getFIO() + " -> " + "средний балл: " + "%.1f%s", person.calcAverageMark(), " ; оценки: " + person.Marks + "(староста)" + "\n");
                } else {
                    System.out.printf(person.getFIO() + " -> " + "средний балл: " + "%.1f%s", person.calcAverageMark(), " ; оценки: " + person.Marks + "\n");
                }

            }
            if (studentsToFire.length() > 2) {
                System.out.println("Студенты к отчислению:" + studentsToFire.substring(0, studentsToFire.length() - 2));
                studentsToFire = " ";
            } else {
                System.out.println("Студентов к отчислению нет");
            }
            System.out.println();
        }
        System.out.println();
    }

    //Перевод студентов из группы в группу
    public void fromGroupToGroup(Student student, Group fromGroup, Group toGroup) {
        fromGroup.expulsionFromGroup(student);
        toGroup.addStudent(student);
    }

    //Отчисление за неуспеваемость
    public void fireStudents() {
        for (Student elem : prepareToFire) {
            elem.groupID = 0;
            elem.groupReference.expulsionFromGroup(elem);
            studentsList.remove(elem);
        }

    }

    //Сохранение обновленных данных
    public void saveData(String studentsFile, String groupsFle) throws IOException {
        try {
            FileWriter refreshStudentsFile = new FileWriter(studentsFile, false);
            FileWriter refreshGroupsFile = new FileWriter(groupsFle, false);
            String stringForWrite;
            for (Student person : studentsList) {
                String id = Integer.toString(person.getID());
                String personGroupID = Integer.toString(person.groupID);
                String headPointer = person.head ? Integer.toString(1) : Integer.toString(0);
                stringForWrite = id + "_" + personGroupID + "_" + headPointer + "_" + person.getFIO() + "_" + person.Marks;
                refreshStudentsFile.write(stringForWrite);
                refreshStudentsFile.append("\n");
            }
            refreshStudentsFile.flush();
            refreshStudentsFile.close();

            for (Group thisGroup : groupsList) {
                String id = Integer.toString(thisGroup.getGroupID());
                String headId = thisGroup.headReference != null ? Integer.toString(thisGroup.headReference.getID()) : "000000";
                String studentsID = "";
                for (Student student : thisGroup.studentsReferences) {
                    studentsID += (Integer.toString(student.getID()) + ",");
                }
                stringForWrite = thisGroup.getGroupNAME() + "_" + id + "_" + headId + "_" + studentsID.substring(0, studentsID.length() - 1);
                refreshGroupsFile.write(stringForWrite);
                refreshGroupsFile.append("\n");
            }
            refreshGroupsFile.flush();
            refreshGroupsFile.close();
        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        }

    }

    //Инициализация выбора старост в группах
    public void headsChoice() {
        System.out.println("Основываясь на данные об успеваемости, предлагаются следующие кандидаты в старосты: ");
        for (Group group : groupsList) {
            String studentsToHead = "";
            double maxAverageMark = 4;
            for (int i = 0; i < group.studentsReferences.size(); i++) {
                if (group.studentsReferences.get(i).numberOfMarks() == 5) {
                    if (group.studentsReferences.get(i).calcAverageMark() > maxAverageMark) {
                        maxAverageMark = group.studentsReferences.get(i).calcAverageMark();
                        studentsToHead += group.studentsReferences.get(i).getFIO() + ", ";
                    }
                } else if (group.studentsReferences.get(i).numberOfMarks() == 4) {
                    if (group.studentsReferences.get(i).calcAverageMark() > maxAverageMark) {
                        maxAverageMark = group.studentsReferences.get(i).calcAverageMark();
                        studentsToHead += group.studentsReferences.get(i).getFIO() + ", ";
                    }
                } else if (studentsToHead.equals("")) {
                    studentsToHead = "кандидатов нет....";
                }
            }
            System.out.println("Группа " + group.getGroupNAME() + ": " + studentsToHead.substring(0, studentsToHead.length() - 2));
            System.out.println();
        }
    }

    //Вывод данных на консоль
    public void inputData() {
        for (Group group: groupsList){
            System.out.println(group.toString());
            System.out.println("Студенты: ");
            for (Student student: group.studentsReferences){
                System.out.println(student.getFIO()+" ID: "+ student.getID());
            }
            System.out.println();
        }
        String waitForAdd = "";
        for (Student student: studentsList){
            if(student.groupID == 0){
                waitForAdd+=student.getFIO()+", ";
            }
        }
        if (waitForAdd.equals("")) System.out.println("Студентов, ожидающих распределения нет");
        else System.out.println("Ожидают распределения в группу: "+waitForAdd.substring(0,waitForAdd.length()-2));
    }

    /**
     * Синхронизация данных полей groupsList studentsList
     * метод к лабораторной не относится
     * при составлении данных о группах было лень перепечатывать номера студентов
     * заставил машину (^_^)
     */
    public void dataSuncronize() {
        for (Student student : studentsList) {
            if (student.groupID != 0) {
                for (Group group : groupsList) {
                    if (student.groupID == group.getGroupID()) {
                        group.addStudent(student);
                        if (student.head) group.headAppointment(student);
                    }
                }
            }
        }
    }


}
