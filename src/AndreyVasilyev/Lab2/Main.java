import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.HashMap;

public class Main
{
    public static void main(String[] args) throws IOException, ParseException, InterruptedException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Automata automat = new Automata();
        while (true)
        {
            switch (automat.State)
            {
                case OFF:
                {
                    automat.getState();
                    if (automat.on()) automat.State = Automata.STATES.WAIT;
                }
                case WAIT:
                {
                    automat.getState();
                    automat.sendToDisplay("\nВнести денежные средства (1) или выключить автомат(2)?");
                    String temp = reader.readLine();
                    if (temp.equals("1"))
                    {
                        automat.prinMenu();
                        automat.coin();
                        automat.State = Automata.STATES.ACCEPT;
                        break;
                    } else if (temp.equals("2"))
                    {
                        if (automat.off()) automat.State = Automata.STATES.OFF;
                        break;
                    }
                }
                case ACCEPT:
                {
                    automat.getState();
                    automat.sendToDisplay("\nВыбрать напиток(1), добавить денежные средства(2) или отменить сеанс(3)?");
                    String temp = reader.readLine();
                    if (temp.equals("1"))
                    {
                        automat.prinMenu();
                        automat.choice();
                        automat.State = Automata.STATES.CHECK;
                        break;
                    } else if (temp.equals("2"))
                    {
                        automat.coin();
                        break;
                    } else if (temp.equals("3"))
                    {
                        automat.cancel();
                        automat.State = Automata.STATES.WAIT;
                        break;
                    }
                }
                case CHECK:
                {
                    automat.getState();
                    if (automat.check() == false)
                    {
                        automat.sendToDisplay("\nНедостаточно денежных средств. Внести дополнительные денежные средства(1) или отменить сеанс(2)?");
                        String temp = reader.readLine();
                        if (temp.equals("1"))
                        {
                            automat.coin();
                            break;
                        } else if (temp.equals("2"))
                        {
                            automat.cancel();
                            automat.State = Automata.STATES.WAIT;
                            break;
                        }
                    } else if (automat.check())
                    {
                        automat.State = Automata.STATES.COOK;
                        break;
                    }
                }
                case COOK:
                {
                    automat.getState();
                    automat.cook();
                    automat.finish();
                    automat.State = Automata.STATES.WAIT;
                    break;
                }
            }
        }
    }
}


class Automata extends Thread
{
    float cash;
    HashMap<String, Float> menu;
    STATES State;
    String choice;

    enum STATES
    {
        OFF, WAIT, ACCEPT, CHECK, COOK
    }

    public Automata() throws IOException, ParseException
    {
        this.choice = "";
        cash = 0;
        this.menu = readMenu();
        this.State = STATES.OFF;
    }

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    //считываем меню из файла и сохраняем в HashMap
    public HashMap<String, Float> readMenu() throws IOException, ParseException
    {
        StringBuilder stringBuilder = new StringBuilder();
        JSONArray menuArr;
        JSONObject resultJSON;
        HashMap<String, Float> menu = new HashMap<>();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("I:/Java projects/Automata/src/menu.json"))))
        {
            String inputLine;
            while ((inputLine = in.readLine()) != null)
            {
                stringBuilder.append(inputLine);
            }
            menuArr = (JSONArray) JSONValue.parseWithException(stringBuilder.toString());
            for (int i = 0; i < menuArr.size(); i++)
            {
                resultJSON = (JSONObject) menuArr.get(i);
                menu.put((String) resultJSON.get("drinkName"), Float.parseFloat((String) resultJSON.get("price")));
            }
        }
        return menu;
    }

    //Работаем с дисплеем
    public void sendToDisplay(String message)
    {
        System.out.print(message);
    }

    //включение автомата
    public boolean on() throws IOException
    {
        String sw = "";
        while (!sw.equals("1"))
        {
            sendToDisplay("\nВключить автомат? (1)");
            sw = reader.readLine();
        }

        return true;
    }

    //выключение автомата
    public boolean off() throws IOException
    {
        String sw = "";
        while (sw.equals("1"))
        {
            sendToDisplay("\nВыключить автомат? (1)\n");
            sw = reader.readLine();
        }
        return true;
    }

    //занесение денег на счёт пользователем
    public void coin() throws IOException
    {
        sendToDisplay("\nВведите наличные");
        cash += (Float.parseFloat(reader.readLine()));
        sendToDisplay("\nДенежные средства: " + cash);
        State = STATES.ACCEPT;
    }

    //отображение меню с напитками и ценами для пользователя
    public void prinMenu()
    {
        menu.forEach((key, value) -> sendToDisplay("\n" + key + " price: " + value));
    }

    //отображение текущего состояния для пользователя
    public void printState()
    {
        sendToDisplay("\nСостояние автомата:" + State.name());
    }

    //выбор напитка пользователем
    public void choice() throws IOException
    {
        String choiceInput = "";
        while (!(menu.containsKey(choiceInput)))
        {
            sendToDisplay("\nВыберите напиток:");
            choiceInput = reader.readLine();
        }
        choice = choiceInput;
    }

    //проверка наличия необходимой суммы
    public boolean check() throws InterruptedException
    {
        if (cash < menu.get(choice))
        {
            sendToDisplay("\nНедостаточно денег для оплаты выбранного напитка");
            return false;

        } else
        {
            return true;
        }
    }

    //отмена сеанса обслуживания пользователем
    public void cancel()
    {
        sendToDisplay("\nОтмена сеанса обслуживания, возврат денежных средств: " + cash);
        cash = 0;
    }

    //имитация процесса приготовления напитка
    public void cook() throws InterruptedException
    {
        cash-=menu.get(choice);
        for (int i = 0; i < 100; i++)
        {
            sleep(100);
            sendToDisplay("#");
        }
        sendToDisplay(" 100%");
        sendToDisplay("\nВаш напиток готов!");
    }

    //завершение обслуживания пользователя
    public void finish()
    {
        if (cash > 0)
        {
            sendToDisplay("\nВаша сдача: " + cash + "\nПриятного аппетита!");
            cash = 0;
        } else sendToDisplay("\nПриятного аппетита!");
    }
}