public class Main
{

    public static void main(String[] args) {

        Circle earth = new Circle();
        earth.setRadius(6378100);
        Circle rope = new Circle();
        rope.setFerence(earth.getFerence()+1.0);
        System.out.println(earth.toString());
        System.out.println(rope.toString());
        System.out.println((rope.getRadius()-earth.getRadius()));

        Circle pool = new Circle();
        pool.setRadius(3);
        PoolFenceCost cost = new PoolFenceCost(pool,1.0);
        System.out.println(cost.toString());
    }
}
class Circle{

    private double Radius;
    private double Ference;
    private double Area;

    public double getArea() {
        return Area;
    }

    public double getFerence() {
        return Ference;
    }

    public double getRadius() {
        return Radius;
    }

    public double getRadiusDifference(Circle firstCirle, Circle seconCircle) {
        return Math.abs(firstCirle.getRadius() - seconCircle.getRadius());
    }

    public void setRadius(double radius) {
        Radius = radius;
        Ference =2*Math.PI*radius;
        Area = Math.PI*radius*radius;
    }

    public void setFerence(double ference) {
        Ference = ference;
        Radius=ference/(Math.PI*2);
        Area=(ference*ference)/(Math.PI*4);
    }

    public void setArea(double area) {
        Area = area;
        Radius=Math.sqrt(area/Math.PI);
        Ference=2*Math.PI* Math.sqrt(area/Math.PI);
    }
}

class PoolFenceCost{

    final static int ConcreteCost = 1000;
    final static int FenceCost = 2000;

    private double concreteValue;
    private double fenceValue;

    @Override
    public String toString()
    {
        return "Стоимость материалов: " + "стоимость бетона- " + concreteValue + " рублей, стоимость ограды- " + fenceValue + " рублей";
    }

    public PoolFenceCost(Circle pool, double concreteWidth) {

        concreteValue =((Math.PI*(Math.pow((pool.getRadius()+concreteWidth),2)))-pool.getArea())*ConcreteCost;
        fenceValue = (pool.getRadius()+concreteWidth)*2*Math.PI*FenceCost;
    }
}