import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by ulizko on 09.08.2017.
 */

class Constellation {
    enum SINGS_OF_ZODIAC {
        ARIES, TAURUS, GEMINI, CANCER, LEO, VIRGO, LIBRA, SCORPIO, SAGITTARIUS, CAPRICORN, AQUARIUS, PISCES
    }

    public SINGS_OF_ZODIAC getSingOfZodiac (LocalDate ld) {
        switch (ld.getMonth()) {
            case JANUARY:
                if(ld.getDayOfMonth() <= 20)
                    return SINGS_OF_ZODIAC.CAPRICORN;
                else if(ld.getDayOfMonth() > 20)
                    return SINGS_OF_ZODIAC.AQUARIUS;
                break;
            case FEBRUARY:
                if(ld.getDayOfMonth() <= 19)
                    return SINGS_OF_ZODIAC.AQUARIUS;
                else if(ld.getDayOfMonth() > 19)
                    return SINGS_OF_ZODIAC.PISCES;
                break;
            case MARCH:
                if(ld.getDayOfMonth() <= 20)
                    return SINGS_OF_ZODIAC.PISCES;
                else if(ld.getDayOfMonth() > 20)
                    return SINGS_OF_ZODIAC.ARIES;
                break;
            case APRIL:
                if(ld.getDayOfMonth() <= 20)
                    return SINGS_OF_ZODIAC.ARIES;
                else if(ld.getDayOfMonth() > 20)
                    return SINGS_OF_ZODIAC.TAURUS;
                break;
            case MAY:
                if(ld.getDayOfMonth() <= 21)
                    return SINGS_OF_ZODIAC.TAURUS;
                else if(ld.getDayOfMonth() > 21)
                    return SINGS_OF_ZODIAC.GEMINI;
                break;
            case JUNE:
                if(ld.getDayOfMonth() <= 21)
                    return SINGS_OF_ZODIAC.GEMINI;
                else if(ld.getDayOfMonth() > 21)
                    return SINGS_OF_ZODIAC.CANCER;
                break;
            case JULY:
                if(ld.getDayOfMonth() <= 22)
                    return SINGS_OF_ZODIAC.CANCER;
                else if(ld.getDayOfMonth() > 22)
                    return SINGS_OF_ZODIAC.LEO;
                break;
            case AUGUST:
                if(ld.getDayOfMonth() <= 21)
                    return SINGS_OF_ZODIAC.LEO;
                else if(ld.getDayOfMonth() > 21)
                    return SINGS_OF_ZODIAC.VIRGO;
                break;
            case SEPTEMBER:
                if(ld.getDayOfMonth() <= 23)
                    return SINGS_OF_ZODIAC.VIRGO;
                else if(ld.getDayOfMonth() > 23)
                    return SINGS_OF_ZODIAC.LIBRA;
                break;
            case OCTOBER:
                if(ld.getDayOfMonth() <= 23)
                    return SINGS_OF_ZODIAC.LIBRA;
                else if(ld.getDayOfMonth() > 23)
                    return SINGS_OF_ZODIAC.SCORPIO;
                break;
            case NOVEMBER:
                if(ld.getDayOfMonth() <= 22)
                    return SINGS_OF_ZODIAC.SCORPIO;
                else if(ld.getDayOfMonth() > 22)
                    return SINGS_OF_ZODIAC.SAGITTARIUS;
                break;
            case DECEMBER:
                if(ld.getDayOfMonth() <= 22)
                    return SINGS_OF_ZODIAC.SAGITTARIUS;
                else if(ld.getDayOfMonth() > 22)
                    return SINGS_OF_ZODIAC.CAPRICORN;
                break;
        }
        return null;
    }
}


class AnalogClock extends JPanel implements Runnable {
    Image imageSec, imageMin, imageHour;
    Image imgBackground;
    long sec, min, hour;
    int radiusLarge, radiusSmall;
    ArrayList<String> adrsImgPlanet;
    String adrsImgConstellation;


    public AnalogClock() {
        Random random = new Random();
        int rnd;

        adrsImgPlanet = new ArrayList<>(Arrays.asList(
                "Planet/Mercury.png", "Planet/Venus.png", "Planet/Earth.png", "Planet/Mars.png",
                "Planet/Jupiter.png", "Planet/Saturn.png", "Planet/Uranus.png", "Planet/Neptune.png",
                "Planet/Pluton.png"));

        rnd = random.nextInt(adrsImgPlanet.size());
        imageSec = new ImageIcon(getClass().getResource(adrsImgPlanet.get(rnd))).getImage();
        adrsImgPlanet.remove(rnd);

        rnd = random.nextInt(adrsImgPlanet.size());
        imageMin = new ImageIcon(getClass().getResource(adrsImgPlanet.get(rnd))).getImage();
        adrsImgPlanet.remove(rnd);

        rnd = random.nextInt(adrsImgPlanet.size());
        imageHour = new ImageIcon(getClass().getResource(adrsImgPlanet.get(rnd))).getImage();
        adrsImgPlanet.remove(rnd);

        Constellation constellation = new Constellation();

        switch (constellation.getSingOfZodiac(LocalDate.now())) {
            case ARIES: adrsImgConstellation = "Constellation/ARIES.jpg";
                break;
            case TAURUS: adrsImgConstellation = "Constellation/TAURUS.jpg";
                break;
            case GEMINI: adrsImgConstellation = "Constellation/GEMINI.jpg";
                break;
            case CANCER: adrsImgConstellation = "Constellation/CANCER.jpg";
                break;
            case LEO: adrsImgConstellation = "Constellation/LEO.jpg";
                break;
            case VIRGO: adrsImgConstellation = "Constellation/VIRGO.jpg";
                break;
            case LIBRA: adrsImgConstellation = "Constellation/LIBRA.jpg";
                break;
            case SCORPIO: adrsImgConstellation = "Constellation/SCORPIO.jpg";
                break;
            case SAGITTARIUS: adrsImgConstellation = "Constellation/SAGITTARIUS.jpg";
                break;
            case CAPRICORN: adrsImgConstellation = "Constellation/CAPRICORN.jpg";
                break;
            case AQUARIUS: adrsImgConstellation = "Constellation/AQUARIUS.jpg";
                break;
            case PISCES: adrsImgConstellation = "Constellation/PISCES.jpg";
                break;
        }

        sec = LocalTime.now().getSecond()*30;
        min = LocalTime.now().getMinute()*30;
        hour = LocalTime.now().getHour()*30;

        (new Thread(this)).start();
    }

    public void run() {
        try {
            for (;;) {
                Thread.sleep(1000/30);

                sec++;

                if((sec % 5000) == 0) {
                    sec = LocalTime.now().getSecond()*30;
                    min = LocalTime.now().getMinute()*30;
                    hour = LocalTime.now().getHour()*30;
                }

                repaint();
            }
        } catch (InterruptedException exc) {
            Thread.currentThread().interrupt();
        }
    }

    public void paint(Graphics graphics) {
        super.paint(graphics);
        Graphics2D g2d = (Graphics2D) graphics;

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        radiusLarge = (Math.min(getWidth(), getHeight()) / 2) - 15;
        radiusSmall = (Math.min(getWidth(), getHeight()) / 2) - 50;

        g2d.drawImage(imgBackground, 0, 0, null);
        g2d.drawOval(15, 15, getWidth() - 30, getHeight() - 30);
        g2d.drawOval(50, 50, getWidth() - 100, getHeight() - 100);

        g2d.setColor(Color.white);
        g2d.translate(getWidth() / 2, getHeight() / 2);
        g2d.rotate(-Math.PI / 2);

        for (int j = 0; j < 60; j++) {
            if ((j % 5) == 0)
                g2d.drawLine(radiusLarge - 1, 0, radiusSmall + 1, 0);
            else
                g2d.drawLine(radiusLarge - 1, 0, radiusSmall + (radiusLarge-radiusSmall) / 2, 0);
            g2d.rotate(6.0 * Math.PI / 180);
        }

        double sec_angle = sec * (6.0/30) * Math.PI / 180;
        double min_angle = (6.0/30) * Math.PI / 180 * (min + sec / 60.0);
        double hour_angle = (30.0/30) * Math.PI/180*(hour+(min+sec/60.0) / 60.0);

        g2d.rotate(sec_angle);
        g2d.drawImage(imageSec, radiusLarge - (imageSec.getWidth(null) / 2), -(imageSec.getHeight(null) / 2), null);

        g2d.rotate(min_angle - sec_angle);
        g2d.drawImage(imageMin, radiusLarge - (imageMin.getWidth(null) / 2), -(imageMin.getHeight(null) / 2), null);

        g2d.rotate(hour_angle - min_angle);
        g2d.drawImage(imageHour, radiusSmall - (imageHour.getWidth(null) / 2), -(imageHour.getHeight(null) / 2), null);
    }
}


public class AnalogClockStart {
    public static void main (String[] args) {
        JWindow jwin = new JWindow();

        jwin.setSize(new Dimension(585, 585));
        jwin.setLocationRelativeTo(null);

        AnalogClock analogClock = new AnalogClock();

        jwin.add(analogClock);
        jwin.setVisible(true);
    }

}

