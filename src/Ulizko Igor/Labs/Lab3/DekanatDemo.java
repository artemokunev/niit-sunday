package com.company;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import static com.sun.deploy.perf.DeployPerfUtil.put;

class Student {
    int ID;
    String FIO;
    Group group;
    ArrayList<Integer> marks = new ArrayList<>();
    int num;
    double rating;

    public Student (int ID, String FIO) {
        this.ID = ID;
        this.FIO = FIO;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void addMark (int mark) {
        num++;
        marks.add(mark);
    }

    public void calcRating() {
        int sum = 0;
        for (int x: marks)
            sum += x;
        rating = (double) sum / num ;
    }

    @Override
    public String toString() {
        return FIO;
    }
}

class Group {
    String title;
    ArrayList<Student> students = new ArrayList<>();
    int num;
    Student head;
    double rating;

    public Group (String title) {
        this.title = title;
    }

    public void addStudent (Student student) {
        num++;
        students.add(student);
    }

    public void setHead () {
        Random random = new Random();
        head = students.get(random.nextInt(num));
    }

    public Student findStudent (String FIO) {
        for (Student student: students)
            if (student.FIO.equals(FIO))
                return student;
        return null;
    }

    public Student findStudent (int ID) {
        for (Student student: students)
            if (student.ID == ID)
                return student;
        return null;
    }

    public void calcRating() {
        int sum = 0;
        for (Student student: students)
            sum += student.rating;
        rating = (double) sum / num;
    }

    public void excStudent (Student student) {
        students.remove(student);
        student.group = null;
        num--;
        if (head == student)
            setHead();
    }

    @Override
    public String toString() {
        return title;
    }
}

class Dekanat {
    ArrayList<Student> students = new ArrayList<>();
    ArrayList<Group> groups = new ArrayList<>();

    public Dekanat() {
        JSONParser parser = new JSONParser();
        try {
            JSONObject object = (JSONObject) parser.parse(new FileReader("students.json"));
            JSONArray jsonStudents = (JSONArray) object.get("students");
                for (Object jsonStudent: jsonStudents) {
                    long id = (long) (((JSONObject) jsonStudent).get("id"));
                    String fio = (String) (((JSONObject) jsonStudent).get("fio"));
                    students.add(new Student((int)id, fio));
                }
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
        try {
            JSONObject object = (JSONObject) parser.parse(new FileReader("groups.json"));
            JSONArray jsonGroups = (JSONArray) object.get("groups");
            for (Object jsonGroup: jsonGroups) {
                String title = (String) (((JSONObject) jsonGroup).get("title"));
                groups.add(new Group(title));
            }
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void setMarks() {
        Random random = new Random();
        for (Student student: students) {
            int num = random.nextInt(10) + 1;
            for (int i = 0; i < num; i++)
                student.addMark((random.nextInt(5))+1);
        }
    }

    public void studentsToGroups() {
        Random random = new Random();
        int i;
        for (Student student: students) {
            i = random.nextInt(groups.size());
            groups.get(i).addStudent(student);
            student.setGroup(groups.get(i));
        }
    }

    public void calcRaitings() {
        for (Student student: students)
            student.calcRating();
        for (Group group: groups)
            group.calcRating();
    }

    public void transferStudents() {
        Random random = new Random();
        int howManyStudents;
        int theStudentID;
        int theGroupID;
        howManyStudents = random.nextInt(students.size() + 1); // сколько студентов
        for (int i = 0; i < howManyStudents; i++) {
            theStudentID = random.nextInt(students.size()); // какой студент
            theGroupID = random.nextInt(groups.size()); // в какую группу
            if (students.get(theStudentID).group == groups.get(theGroupID))
                continue;
            students.get(theStudentID).group.excStudent(students.get(theStudentID));
            groups.get(theGroupID).addStudent(students.get(theStudentID));
            students.get(theStudentID).setGroup(groups.get(theGroupID));
        }
    }

    public void excStudents () {
        Iterator<Student> studentIterator = students.iterator();
        while (studentIterator.hasNext()) {
            Student student = studentIterator.next();
            if (student.rating < 2.5) {
                student.group.excStudent(student);
                studentIterator.remove();
            }
        }
    }

    public void fileOutput () {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonStudent;
        JSONArray jsonStudents = new JSONArray();
        for (Student student: students) {
            jsonStudent = new JSONObject();
            jsonStudent.put("id", student.ID);
            jsonStudent.put("fio", student.FIO);
            jsonStudent.put("group", student.group);
            jsonStudent.put("raiting", new DecimalFormat("#0.0").format(student.rating));
            jsonStudents.add(jsonStudent);
        }
        jsonObject.put("students", jsonStudents);
        try (FileWriter writer = new FileWriter("students.json")){
            writer.write(jsonObject.toJSONString());
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        jsonObject.clear();
        JSONObject jsonGroup;
        JSONArray jsonGroups = new JSONArray();
        for (Group group: groups) {
            jsonGroup = new JSONObject();
            jsonGroup.put("title", group.title);
            jsonGroup.put("num", group.num);
            jsonGroup.put("head", group.head);
            jsonGroup.put("raiting", new DecimalFormat("#0.0").format(group.rating));
            jsonGroups.add(jsonGroup);
        }
        jsonObject.put("groups", jsonGroups);
        try (FileWriter writer = new FileWriter("groups.json")){
            writer.write(jsonObject.toJSONString());
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void setHeads() {
        for (Group group: groups) {
            if (group.num == 0)
                continue;
            group.setHead();
        }
    }

    public void consoleOutput () {
        for (Student student: students) {
            System.out.println("Студент: " + student.FIO);
            System.out.println("Порядковый №: " + student.ID);
            System.out.println("Рейтинг: " + new DecimalFormat("#0.0").format(student.rating));
            System.out.println("Группа: " + student.group);
            System.out.println("Оценок: " + student.num);
            System.out.print("Оценки: ");
            for (int mark : student.marks)
                System.out.print(mark + " ");
            System.out.println();
            System.out.println();
        }
        for (Group group: groups) {
            System.out.println("Группа: " + group.title);
            System.out.println("Кол-во: " + group.num);
            System.out.println("Староста: " + group.head);
            System.out.println("Рейтинг: " + new DecimalFormat("#0.0").format(group.rating));
            System.out.println("Состав: ");
            for (Student student : group.students)
                System.out.println("   " + student);
            System.out.println();
            System.out.println();
        }
    }

}

public class DekanatDemo {
    public static void main(String[] args) {
	    Dekanat dekanat = new Dekanat();
	    dekanat.studentsToGroups();
        dekanat.setMarks();
        dekanat.calcRaitings();
        dekanat.setHeads();
        dekanat.consoleOutput();
        System.out.println("################################");
        dekanat.transferStudents();
        dekanat.consoleOutput();
        System.out.println("################################");
        dekanat.excStudents();
        dekanat.consoleOutput();
        dekanat.fileOutput();
    }
}
