package com.company;

import javax.swing.*;
import javax.swing.Timer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.*;

import java.util.*;


class AutomataGUI implements ActionListener, ListSelectionListener {
    JFrame jfrm;

    JLabel jlabState, nameJava;

    JList<String> jlstMenu;
    JScrollPane jscrlp;
    DefaultListModel model;

    JButton jbtnCancel, jbtnChoice, jbtnSwitch;

    JPanel jpnlCoin;
    JButton[] btnCoin;

    Automata automata;

    Timer timerSimulateCooking;

    String[] adrsFrmsLogoNiit = {
            "NIITAnimation/icon1.png", "NIITAnimation/icon2.png", "NIITAnimation/icon3.png", "NIITAnimation/icon4.png", "NIITAnimation/icon5.png", "NIITAnimation/icon6.png",
            "NIITAnimation/icon7.png", "NIITAnimation/icon8.png", "NIITAnimation/icon9.png", "NIITAnimation/icon10.png", "NIITAnimation/icon11.png", "NIITAnimation/icon12.png",
            "NIITAnimation/icon13.png", "NIITAnimation/icon14.png", "NIITAnimation/icon15.png", "NIITAnimation/icon16.png", "NIITAnimation/icon17.png", "NIITAnimation/icon18.png",
            "NIITAnimation/icon19.png", "NIITAnimation/icon20.png", "NIITAnimation/icon21.png", "NIITAnimation/icon22.png", "NIITAnimation/icon23.png", "NIITAnimation/icon24.png",
            "NIITAnimation/icon25.png", "NIITAnimation/icon26.png", "NIITAnimation/icon27.png", "NIITAnimation/icon28.png", "NIITAnimation/icon29.png", "NIITAnimation/icon30.png",
            "NIITAnimation/icon31.png", "NIITAnimation/icon32.png", "NIITAnimation/icon33.png", "NIITAnimation/icon34.png", "NIITAnimation/icon35.png", "NIITAnimation/icon36.png",
            "NIITAnimation/icon37.png", "NIITAnimation/icon38.png", "NIITAnimation/icon39.png", "NIITAnimation/icon40.png", "NIITAnimation/icon41.png", "NIITAnimation/icon42.png"
    };
    String[] adrsFrmsLogoJava = {
            "logoJava/frame1.png", "logoJava/frame2.png", "logoJava/frame3.png", "logoJava/frame4.png", "logoJava/frame5.png", "logoJava/frame6.png",
            "logoJava/frame7.png", "logoJava/frame8.png", "logoJava/frame9.png", "logoJava/frame10.png", "logoJava/frame11.png", "logoJava/frame12.png",
            "logoJava/frame13.png", "logoJava/frame14.png", "logoJava/frame15.png", "logoJava/frame16.png", "logoJava/frame17.png", "logoJava/frame18.png",
            "logoJava/frame19.png", "logoJava/frame20.png", "logoJava/frame21.png", "logoJava/frame22.png", "logoJava/frame23.png", "logoJava/frame24.png",
            "logoJava/frame25.png", "logoJava/frame26.png", "logoJava/frame27.png"
    };

    Animation animationLogoNiit = new Animation(adrsFrmsLogoNiit, true);
    Animation animationLogoJava = new Animation(adrsFrmsLogoJava, false);



    AutomataGUI(Automata automata) {
        this.automata = automata;

        jfrm = new JFrame();
        jfrm.setLayout(new GridBagLayout());
        jfrm.setSize(500, 600);
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jfrm.setLocationRelativeTo(null);
        jfrm.setResizable(false);

        jlabState = new JLabel();
        jlabState.setPreferredSize(new Dimension(300, 70));
        jlabState.setOpaque(true);
        jlabState.setBackground(Color.BLUE);
        jlabState.setForeground(Color.WHITE);
        jlabState.setFont(new Font("courier new", Font.PLAIN, 20));
        jlabState.setHorizontalAlignment(JLabel.CENTER);

        nameJava = new JLabel();
        nameJava.setIcon(new ImageIcon(getClass().getResource("nameJava.png")));

        btnCoin = new JButton[4];

        btnCoin[0] = new JButton();
        btnCoin[0].setIcon(new ImageIcon(getClass().getResource("rubFront5.png")));
        btnCoin[0].setPressedIcon(new ImageIcon(getClass().getResource("rubRear5.png")));
        btnCoin[0].setActionCommand("coin5");

        btnCoin[1] = new JButton();
        btnCoin[1].setIcon(new ImageIcon(getClass().getResource("rubFront10.png")));
        btnCoin[1].setPressedIcon(new ImageIcon(getClass().getResource("rubRear10.png")));
        btnCoin[1].setActionCommand("coin10");

        btnCoin[2] = new JButton();
        btnCoin[2].setIcon(new ImageIcon(getClass().getResource("rubFront15.png")));
        btnCoin[2].setPressedIcon(new ImageIcon(getClass().getResource("rubRear15.png")));
        btnCoin[2].setActionCommand("coin15");

        btnCoin[3] = new JButton();
        btnCoin[3].setIcon(new ImageIcon(getClass().getResource("rubFront20.png")));
        btnCoin[3].setPressedIcon(new ImageIcon(getClass().getResource("rubRear20.png")));
        btnCoin[3].setActionCommand("coin20");


        jpnlCoin = new JPanel();
        jpnlCoin.setLayout(new GridLayout(1, 4));

        for (JButton btn : btnCoin) {
            btn.setBorderPainted(false);
            btn.setFocusPainted(false);
            btn.setContentAreaFilled(false);
            btn.setPreferredSize(new Dimension(75, 75));
            btn.addActionListener(this);
            jpnlCoin.add(btn);
        }

        jbtnSwitch = new JButton();
        jbtnSwitch.setIcon(new ImageIcon(getClass().getResource("off.png")));
        jbtnSwitch.setBorderPainted(false);
        jbtnSwitch.setFocusPainted(false);
        jbtnSwitch.setContentAreaFilled(false);
        jbtnSwitch.setActionCommand("switch");

        model = new DefaultListModel();

        jlstMenu = new JList<String>();
        jlstMenu.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jlstMenu.setFont(new Font("courier new", Font.PLAIN, 20));
        jlstMenu.setModel(model);

        jscrlp = new JScrollPane(jlstMenu);
        jscrlp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "menu"));
        jscrlp.setPreferredSize(new Dimension(170, 200));

        jbtnCancel = new JButton("CANCEL");
        jbtnCancel.setPreferredSize(new Dimension(100, 100));
        jbtnCancel.setEnabled(false);

        jbtnChoice = new JButton("CHOICE");
        jbtnChoice.setPreferredSize(new Dimension(100, 100));
        jbtnChoice.setEnabled(false);

        animationLogoJava.setPreferredSize(new Dimension(150,150));

        timerSimulateCooking = new Timer(400, this);
        timerSimulateCooking.setActionCommand("timerSimulateCooking");

        jfrm.add(jlabState, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        jfrm.add(animationLogoNiit, new GridBagConstraints(2, 0, 1, 2, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
        jfrm.add(jpnlCoin, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        jfrm.add(jscrlp, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.NORTH, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        jfrm.add(jbtnChoice, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(15, 35, 0, 0), 0, 0));
        jfrm.add(jbtnCancel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(15, 0, 0, 0), 0, 0));
        jfrm.add(jbtnSwitch, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.NORTH, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        jfrm.add(animationLogoJava,new GridBagConstraints(0, 3, 2, 2, 0.0, 0.0,
                GridBagConstraints.NORTH, GridBagConstraints.NONE,
                new Insets(0, 50, 0, 0), 0, 0));
        jfrm.add(nameJava,new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));

        jbtnSwitch.addActionListener(this);
        jlstMenu.addListSelectionListener(this);
        jbtnCancel.addActionListener(this);
        jbtnChoice.addActionListener(this);

        jfrm.pack();
        jfrm.setVisible(true);
    }



    public void actionPerformed(ActionEvent ae) {
        switch (ae.getActionCommand()) {
            case "switch":
                automata.stateSwitch = !(automata.stateSwitch);
                if (automata.stateSwitch == true) {
                    automata.on();
                    jbtnSwitch.setIcon(new ImageIcon(getClass().getResource("on.png")));
                    for (String str : automata.menu)
                        model.addElement(str);

                    animationLogoNiit.setVisible(true);
                    animationLogoNiit.enable = true;
                    animationLogoNiit.timer.start();

                    jlabState.setText(String.valueOf(STATES.WAITING));
                } else {
                    automata.off();
                    model.removeAllElements();
                    jbtnSwitch.setIcon(new ImageIcon(getClass().getResource("off.png")));

                    animationLogoNiit.timer.stop();
                    animationLogoNiit.enable = false;
                    animationLogoNiit.setVisible(false);

                    animationLogoJava.repaint();
                    jlabState.setText("");
                }
                jbtnChoice.setEnabled(automata.stateSwitch);
                jbtnCancel.setEnabled(automata.stateSwitch);
                break;

            case "coin5":
                if(automata.stateSwitch == false)
                    break;
                automata.coin(5);
                jlabState.setText("CASH: " + automata.cash + ((jlstMenu.getSelectedIndex() != -1) ? "  COST: " + automata.prices.get(jlstMenu.getSelectedIndex()) : " "));
                break;

            case "coin10":
                if(automata.stateSwitch == false)
                    break;
                automata.coin(10);
                jlabState.setText("CASH: " + automata.cash + ((jlstMenu.getSelectedIndex() != -1) ? "  COST: " + automata.prices.get(jlstMenu.getSelectedIndex()) : " "));
                break;

            case "coin15":
                if(automata.stateSwitch == false)
                    break;
                automata.coin(15);
                jlabState.setText("CASH: " + automata.cash + ((jlstMenu.getSelectedIndex() != -1) ? "  COST: " + automata.prices.get(jlstMenu.getSelectedIndex()) : " "));
                break;

            case "coin20":
                if(automata.stateSwitch == false)
                    break;
                automata.coin(20);
                jlabState.setText("CASH: " + automata.cash + ((jlstMenu.getSelectedIndex() != -1) ? "  COST: " + automata.prices.get(jlstMenu.getSelectedIndex()) : " "));
                break;

            case "CANCEL":
                automata.cancel();
                jlstMenu.clearSelection();
                jlabState.setText(String.valueOf(STATES.WAITING));
                break;

            case "CHOICE":
                if(jlstMenu.getSelectedIndex() == -1) {
                    jlabState.setText("DRINK NOT SELECTED");
                    return;
                }
                if(automata.check(jlstMenu.getSelectedIndex()) == false)
                    jlabState.setText("NOT ENOUGH FUNDS");
                else {
                    jlabState.setText(String.valueOf(STATES.COOKING));

                    jbtnSwitch.setEnabled(false);
                    jlstMenu.setEnabled(false);
                    jbtnCancel.setEnabled(false);
                    jbtnChoice.setEnabled(false);
                    for (JButton btn : btnCoin)
                        btn.setEnabled(false);

                    animationLogoJava.enable = true;
                    animationLogoJava.timer.start();

                    timerSimulateCooking.start();
                }
                break;

            case "timerSimulateCooking":
                if(animationLogoJava.timer.isRunning())
                    jlabState.setText(jlabState.getText() + "#");
                else {
                    timerSimulateCooking.stop();

                    int rest = automata.finish(jlstMenu.getSelectedIndex());
                    if(rest != 0)
                        jlabState.setText("DONE. YOUR REST: " + rest);
                    else
                        jlabState.setText("DONE");

                    jbtnSwitch.setEnabled(true);
                    jlstMenu.setEnabled(true);
                    jbtnCancel.setEnabled(true);
                    jbtnChoice.setEnabled(true);
                    for (JButton btn : btnCoin)
                        btn.setEnabled(true);

                    jlstMenu.clearSelection();
                }
        }

    }



    public void valueChanged(ListSelectionEvent le) {
        int idxChoiceLst = jlstMenu.getSelectedIndex();
        if(idxChoiceLst != -1)
            jlabState.setText("CASH: " + automata.cash + "  COST: " + automata.prices.get(idxChoiceLst));
    }
}



class Animation extends JPanel implements ActionListener {
    Image image;

    boolean clean;
    boolean replay;
    boolean enable;

    String[] addressesFrames;
    int idxFrame = -1;

    Timer timer = new Timer(75, this);



    Animation(String[] addressesFrames, boolean replay) {
        this.addressesFrames = addressesFrames;
        this.replay = replay;
    }


    public void paint(Graphics g) {
        if(enable) {
            super.paint(g);
            if (clean) {
                Graphics2D g2d = (Graphics2D) g;
                g2d.setColor(getBackground());
                g2d.fillRect(0, 0, getWidth(), getHeight());
                clean = false;
                timer.restart();
            } else if(idxFrame != -1) {
                image = new ImageIcon(getClass().getResource(addressesFrames[idxFrame])).getImage();
                g.drawImage(image, 0, 0, null);
            }
        }
    }


    public void actionPerformed(ActionEvent ae) {
        idxFrame++;
        if(idxFrame == addressesFrames.length) {
            timer.stop();
            clean = true;
            idxFrame = -1;
            if(replay == false) {
                enable = false;
                return;
            }
            repaint();
        }
        else
            repaint();
    }
}


enum STATES {
    WAITING, COOKING
}


class Automata  {
    int cash;

    ArrayList<String> menu = new ArrayList<>();
    ArrayList<Integer> prices = new ArrayList<>();

    boolean stateSwitch;
    STATES state;

    public void on() {
        String str = null;
        try (BufferedReader menuFile = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("menu.txt")))) {
            while ((str = menuFile.readLine()) != null)
                try {
                    prices.add(Integer.parseInt(str));
                } catch (NumberFormatException e) {
                    menu.add(str);
                }
        } catch (IOException exc) {
            System.out.println("ОШИБКА ВВОДА-ВЫВОДА ИЗ ФАЙЛА МЕНЮ");
        }
        state = STATES.WAITING;
    }

    public void off() {
        menu.clear();
        prices.clear();
        cash = 0;
    }

    public void coin (int coinUser) {
        cash += coinUser;
    }

    public void cancel() {
        cash = 0;
        state = STATES.WAITING;
    }

    boolean check(int numUsersChoice) {
        if (cash >= prices.get(numUsersChoice)) {
            return true;
        }
        else {
            state = STATES.WAITING;
            return false;
        }
    }

    int finish(int numUsersChoice) {
        int sub;
        sub = cash - prices.get(numUsersChoice);
        cash = 0;
        state = STATES.WAITING;
        return sub;
    }
}

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Automata automata = new Automata();
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new AutomataGUI(automata);

            }
        });
    }
}