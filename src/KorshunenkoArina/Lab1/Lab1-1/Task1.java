public class Task1 {
    static int steps;

    public static void main(String args[]) {
        int maxSteps = 0;
        int maxNum = 1;

        for (int n = 1; n <= 1000000; n++) {
            Collatz(n);
            if (steps > maxSteps) {
                maxSteps = steps;
                maxNum = n;
            }
            steps = 0;
        }

        System.out.println("Число с наибольшей последовательностью Коллтаца: " + maxNum );
        System.out.println("Число шагов: " + maxSteps);
    }

    public static void Collatz(int i) {
        if (i == 1) return;
        steps++;
        if (i % 2 == 0)
            Collatz(i / 2);
        else if (i % 2 == 1)
            Collatz (i * 3 + 1);
    }

}