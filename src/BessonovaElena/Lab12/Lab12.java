import java.util.Scanner;

import static java.lang.Double.parseDouble;

class Sqrt {

    double arg;

    Sqrt(double arg) {
        this.arg = arg;
    }

    double average(double x, double y) {
        return (x + y) / 2.0;
    }

    boolean good(double guess, double x, int delta) {
        double d = Math.pow(10.0, -1 * (double) delta);
        return Math.abs(guess * guess - x) < d;
    }

    double improve(double guess, double x) {
        return average(guess, x / guess);
    }

    double iter(double guess, double x, int delta) {
        if (good(guess, x, delta))
            return guess;
        else
            return iter(improve(guess, x), x, delta);
    }

    public double calc(int delta) {

        return iter(1.0, arg, delta);
    }
}

public class Lab12 {
    public static void main(String[] args) {
        double val = parseDouble(args[0]);
        Sqrt sqrt = new Sqrt(val);

        Scanner in = new Scanner(System.in);
        System.out.println("Ввести порядок точности: ");
        int delta = in.nextInt();

        double result = sqrt.calc(delta);
        System.out.println("Sqrt of " + val + "=" + result);
    }
}
