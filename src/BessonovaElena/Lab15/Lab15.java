
public class Lab15 {

    public static void main(String[] args) {
        String[] nZero = {"   ***   ",
                          "  *   *  ",
                          " *     * ",
                          " *     * ",
                          " *     * ",
                          "  *   *  ",
                          "   ***   "};

        String[] nOne = {"  *  ",
                         " **  ",
                         "  *  ",
                         "  *  ",
                         "  *  ",
                         "  *  ",
                         " *** "};

        String[] nTwo = {" *** ",
                         "*   *",
                         "*  * ",
                         "  *  ",
                         " *   ",
                         "*    ",
                         "*****"};

        String[] nThree = {"  ***  ",
                           " *   * ",
                           "     * ",
                           "   *   ",
                           "     * ",
                           " *   * ",
                           "  ***  "};

        String[] nFour = {"     *   ",
                          "   * *   ",
                          "  *  *   ",
                          " *   *   ",
                          " ******* ",
                          "     *   ",
                          "     *   "};

        String[] nFive = {" ***** ",
                          " *     ",
                          " *     ",
                          " ****  ",
                          "     * ",
                          " *   * ",
                          "  ***  "};

        String[] nSix = {"   *** ",
                         "  *    ",
                         " *     ",
                         " ****  ",
                         " *   * ",
                         " *   * ",
                         "  ***  "};

        String[] nSeven = {" ***** ",
                           "     * ",
                           "     * ",
                           "   *   ",
                           "  *    ",
                           " *     ",
                           " *     "};

        String[] nEight = {"  ***  ",
                           " *   * ",
                           " *   * ",
                           "   *   ",
                           " *   * ",
                           " *   * ",
                           "  ***  "};

        String[] nNine = {"  ***  ",
                          " *   * ",
                          " *   * ",
                          "  **** ",
                          "     * ",
                          " *   * ",
                          "  ***  "};

        long number = Long.parseLong(args[0]);
        long tempNumber = number;
        int count = 0;
        while (tempNumber > 0) {
            tempNumber /= 10;
            count++;
        }
        long[] numberArray = new long[count];
        for (int i = count - 1; i >= 0; i--) {
            numberArray[i] = number % 10;
            number /= 10;

        }

        String[] resultArr = {"", "", "", "", "", "", ""};

        for (int j = 0; j < 7; j++) {
            for (int i = 0; i < count; i++) {
                switch ((int)numberArray[i]) {
                    case 0:
                        resultArr[j] = resultArr[j].concat(nZero[j]);
                        break;
                    case 1:
                        resultArr[j] = resultArr[j].concat(nOne[j]);
                        break;
                    case 2:
                        resultArr[j] = resultArr[j].concat(nTwo[j]);
                        break;
                    case 3:
                        resultArr[j] = resultArr[j].concat(nThree[j]);
                        break;
                    case 4:
                        resultArr[j] = resultArr[j].concat(nFour[j]);
                        break;
                    case 5:
                        resultArr[j] = resultArr[j].concat(nFive[j]);
                        break;
                    case 6:
                        resultArr[j] = resultArr[j].concat(nSix[j]);
                        break;
                    case 7:
                        resultArr[j] = resultArr[j].concat(nSeven[j]);
                        break;
                    case 8:
                        resultArr[j] = resultArr[j].concat(nEight[j]);
                        break;
                    case 9:
                        resultArr[j] = resultArr[j].concat(nNine[j]);
                        break;
                }
            }
            System.out.println(resultArr[j]);
        }
    }
}
