import java.util.Scanner;

public class Lab14 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String source = in.nextLine();

        String[] sourceArray = source.split(",");
        int[] numberArray = new int[sourceArray.length + 1];
        for (int i = 0; i < sourceArray.length; i++) {
            numberArray[i] = Integer.valueOf(sourceArray[i]).intValue();
        }
        String tempString = "";

        for (int i = 0; i < sourceArray.length; i++) {
            int counter = 0;
            if ((i != 0) && (i != sourceArray.length - 1)) {
                if ((numberArray[i - 1] == numberArray[i] - 1) && (numberArray[i + 1] == numberArray[i] + 1)) {
                    counter++;
                    tempString = tempString.concat("-");
                }
                if (counter == 0) {
                    tempString = tempString.concat(Integer.toString(numberArray[i])).concat(",");
                }

            } else {
                tempString = tempString.concat(Integer.toString(numberArray[i]));
                if (i != sourceArray.length - 1) {
                    tempString=tempString.concat(",");
                }
            }

        }
        tempString = tempString.replaceAll(",-", "-");
        while (tempString.contains("--")) {
            tempString = tempString.replaceAll("--", "-");
        }
        System.out.println(tempString);

    }
}
