public class Kollatz{
	public static void main(String[] args){
		KollatzComputation kc = new KollatzComputation();
		int maxStep = 0;
		int maxArg = 0;
		//int stepCount = kc.computeKollatz(i);
		//System.out.println("\nCount of steps: "+stepCount);
		for(int i = 1; i <= 1000000; i++){
			int stepCount = kc.computeKollatz(i);
			if(stepCount > maxStep){
				maxStep = stepCount;
				maxArg = i;
			}
		}
		System.out.println("Max chain for number: "+maxArg);
		System.out.println("Count of steps: "+maxStep);
	}
}
class KollatzComputation {
	public int computeKollatz(int i){
		int stepCount = 0;
		//System.out.println("i = "+i);
		while(i>1){
			stepCount++;
			if((i % 2) == 0){
				i = i/2;
				//System.out.print(i+" ");
			}else {
				i = i * 3 + 1;
				//System.out.print(i+" ");
			}
		}
		//System.out.println();
		return stepCount;
		//System.out.println("\nCount of steps: "+stepCount); //Это сопли
	}
}