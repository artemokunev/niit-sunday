/**
 * Created by user.1 on 06.08.2017.
 */
public class Automata {
    enum STATES {OFF,WAIT,ACCEPT,CHECK,COOK}
    private int cash;
    private String[] menu = {"Americano","Capuccino","Latte"};
    private int[] prices = {30,35,40};
    private STATES state;

    Automata(){
        cash = 0;
        state = STATES.OFF;
    }

    public STATES getState() {
        return state;
    }
    public void on(){
        if(state == STATES.OFF){
            state = STATES.WAIT;
            return;
        }else return; //Возврат без изменения состояния - кнопка включения заблокирована
    }
    public void off(){
        if(state == STATES.WAIT){
            state = STATES.OFF;
            return;
        }else return; //Возврат без изменения состояния - кнопка выключения заблокирована
    }
    public void coin(int value) {
        if (state == STATES.WAIT || state == STATES.ACCEPT) {
            if (value > 0) {
                cash = cash + value;
                state = STATES.ACCEPT;
            } else return; //Забирать деньги из автомата не позволяем
        } else return; //Не принимаем деньги если не ждём их. Возможно надо возвращать ошибку
    }
    public String printMenu(){
        StringBuilder result = new StringBuilder();
        for(int i=0;i<menu.length;i++){
            result.append(menu[i]+' '+prices[i]+'\n');
        }
        return result.toString();
    }
    public String printState(){
        return state.toString();
    }
    //Возвращает текущий баланс
    public String printCash(){
        Integer retValue = new Integer(cash);
        return retValue.toString();
    }
    //choice() возвращает сумму сдачи, либо сумму, которую следует вернуть клиенту если средств на счету недостаточно
    public int choice(int menuItem){
        //Сначала надо реализовать check(), cancel(), cook() и finish()
        if(state == STATES.ACCEPT){
            if(check(menuItem)){
                state = STATES.CHECK;
                return cook(menuItem);
            }else return cancel();
        }else return 0;
    }
    public boolean check(int menuItem){
        if(prices[menuItem] > cash) {
            return false;
        }else return true;
    }
    //cancel() возвращает сумму денег, которую следует вернуть клиенту
    public int cancel(){
        if(state == STATES.ACCEPT || state == STATES.CHECK){
            state = STATES.WAIT;
            return cash;
        }
        else return 0;
    }
    public void resetCash(){
        cash = 0;
    }
    /*
    cook() "Готовит напиток", меняет состояние автомата, списывает средства со счёта в размере стоимости напитка,
    вызывает finish() по завершении работы
    Возвращает сумму сдачи
     */
    private int cook(int menuItem){
        if(state == STATES.CHECK){
            state = STATES.COOK;
            cash = cash - prices[menuItem];
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return finish();
        }else return 0;

    }
    /*
    finish() переводит автомат в статус WAIT и возвращает сумму сдачи
     */
    private int finish(){
        if(state == STATES.COOK){
            state = STATES.WAIT;
            return cash;
        } else return 0;
    }
}
