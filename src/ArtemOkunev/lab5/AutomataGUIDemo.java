/**
 * Created by user.1 on 06.08.2017.
 */
/*
Этот проект должен включать класс, реализующий функции кофейного автомата и реализацию графического
интерфейса автомата. Запускать GUI желательно в отдельном потоке. Для пущей красоты следует сделать
визуально отображение процесса выполнения операций автомата управляемым со стороны потока основного
 класса.
 */
public class AutomataGUIDemo {
    public static void main(String[] args) {

        /*
        Класс GUI должен отображать интерфейс пользователя и вызывать методы автомата.
        Автомат должен выполнять методы и сигнализировать о своём состоянии. Например, событие
        включения должно как-то отображаться. Процесс готовки кофе должен иметь визуальное сопровождение.
        Необходимо обеспечить взаимодействие между классами.
         */
        AutomataGUI ag = new AutomataGUI();
        Thread tg = new Thread(ag);
        tg.start(); //Запускается GUI

    }
}
