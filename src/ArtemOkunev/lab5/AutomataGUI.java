import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.*;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Button;

import java.util.ArrayList;

/**
 * Created by Artem Okunev on 06.08.2017.
 */
public class AutomataGUI extends Application implements Runnable {

/*    public void setAutomataObject(AutomataThread a){
        this.atr = a;
    }*/
    //public void setStatusMessage()
    @Override
    public void start(Stage stage) throws Exception {
        AutomataThread atr = new AutomataThread();
        Thread ta;
        ta = new Thread(atr);//Запускается AutomataThread
        ta.start();
        //System.out.println("AutomataThread started");

        Button onBtn = new Button("On");
        Button offBtn = new Button("Off");
        //Это дисплей-индикатор
        TextArea mon1 = new TextArea();
        mon1.setLayoutX(10);
        mon1.setLayoutY(10);
        mon1.setCursor(Cursor.TEXT);
        mon1.setStyle("-fx-background-radius:10;-fx-border-radius:10;-fx-background-color:#ffefd5;-fx-border-width:1pt;-fx-border-color:#000000;-fx-fontweight:bold;-fx-font-size:14pt; -fx-font-family:Georgia; -fx-fontstyle:italic");
        mon1.setPrefSize(200,50);
        //mon1.setText(atr.toString());
        mon1.setWrapText(true);
        mon1.setEditable(true);
        //Это кнопка включения
        onBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                final Task task = new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        atr.on();
                        if(atr.getState() == Automata.STATES.WAIT){
                            updateMessage("Готов");
                        }
                        return null;
                    }
                    @Override
                    protected void updateMessage(String msg){
                        mon1.setText(msg);
                    }
                };
                Service service=new Service<Void>() {
                    @Override
                    protected Task createTask() {
                        return task;
                    }};
                service.start();
            }
        });
        offBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                final Task taskoff = new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        atr.off();
                        if(atr.getState() == Automata.STATES.OFF) {
                            updateMessage("Status: OFF");
                        }
                        return null;
                    }
                    @Override
                    protected void updateMessage(String msg){
                        if(msg.equals("Status: OFF")){
                            mon1.clear();
                            //mon1.setText(msg);
                        }
                    }
                };
                Service serviceoff;
                serviceoff = new Service<Void>() {
                    @Override
                    protected Task createTask() {
                        return taskoff;
                    }};
                serviceoff.start();
            }
        });
        /* TODO: 15.08.2017 Добавить элементы интерфейса:
        * Меню выбора напитков.
        * Монетоприёмник
        * Отображение текущего баланса
        * Способ отмены заказа
        * Выбор напитка. С проверкой баланса.
        * Отображение готовности напитка
        * Отображение возврата денег
        * */
        HBox topPanel = new HBox(); //Панель кнопок включения-выключения
        topPanel.setSpacing(10);
        topPanel.setAlignment(Pos.CENTER_RIGHT);
        topPanel.getChildren().addAll(onBtn,offBtn);
        HBox menuPanel = new HBox(); //Панель выбора напитка.
        menuPanel.setSpacing(5);
        VBox leftMenuPanel = new VBox(); //Левая колонка с кнопками меню
        VBox rightMenuPanel = new VBox();//Правая колонка с кнопками меню
        /*Здесь нам надо получить меню из Automata, полученный String распарсить на строки,
        затем сделать из них кнопки какие-то.
        Нарисуем 10 кнопок в 2 ряда, по ним расставим пункты меню. Лишние кнопки будут пустые
        Если напитков больше, чем кнопок, то лишние теряем
         */

        Separator sepV = new Separator();
        sepV.setOrientation(Orientation.VERTICAL);
        VBox coinPanel = new VBox(); //Панель монетоприёмника
        Button insertCoin = new Button("Внести 5 рублей");
        insertCoin.setOnAction(event -> {
            if(atr.getState()== Automata.STATES.ACCEPT||atr.getState()== Automata.STATES.WAIT) {
                atr.coin(5);
                mon1.setText("Внесено " + atr.printCash() + " рублей");
            }
        });
        Button cancelButton = new Button("Вернуть деньги");
        cancelButton.setOnAction(event -> {
            if(atr.getState()== Automata.STATES.ACCEPT||atr.getState()== Automata.STATES.WAIT) {
                mon1.setText("К возврату "+atr.printCash()+" рублей");
                atr.resetCash();
            }
        });
        coinPanel.getChildren().addAll(insertCoin,cancelButton);
        ProgressBar pg = new ProgressBar(); //Прогресс приготовления кофе
        VBox pgPanel = new VBox(); //Панель прогрессбара
        final DoubleProperty progress = new SimpleDoubleProperty(0.0);
        pg.progressProperty().bind(progress);
        pgPanel.getChildren().addAll(pg);
        progress.setValue(0.0);
        EventHandler menuEvent = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                EventTarget eventTarget = event.getTarget();
                Object eventSource = event.getSource();
                /*Здесь надо выполнить ряд проверок чтобы choice не запускалась если денег недостаточно или автомат
                вообще не в том состоянии
                 */
                if(atr.getState() == Automata.STATES.ACCEPT || atr.getState() == Automata.STATES.WAIT) {//Автомат принял монетки
                    if (atr.check(Integer.parseInt(((Button) eventSource).getId()))) {//Проверяем достаточно ли денег внесено
                        final Task taskChoice = new Task() {/*taskChoice запускает taskCook, а потом занимается обновлением
                        прогрессбара, пока taskChoice не завершится. Крутить прогрессбар не в таске не получается - он обновляется
                        только после завершения обработчика*/
                            @Override
                            protected Void call() throws Exception {
                                //System.out.println("taskChoice started");
                                final Task taskCook = new Task() {/*
                                */
                                    @Override
                                    protected Void call() throws Exception {
                                        //System.out.println("taskCook started");
                                        atr.choice(Integer.parseInt(((Button) eventSource).getId()));
                                        updateMessage("Возьмите напиток. ");

                                        return null;
                                    }
                                    @Override
                                    protected void updateMessage(String msg) {
                                        mon1.setText(msg+"Сдача "+atr.printCash()+" рублей");
                                    }


                                };
                                Service serviceCook = new Service() {
                                    @Override
                                    protected Task createTask() {
                                        return taskCook;
                                    }
                                };
                                serviceCook.start();
                                while (atr.getState() != Automata.STATES.WAIT) {
                                    if (atr.getState() == Automata.STATES.COOK) {
                                        //System.out.println(atr.getState());
                                        try {
                                            Thread.sleep(1000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        updateProgress();
                                    }
                                }
                                updateProgress(0.0);
                                return null;
                            }

                            protected void updateProgress(){
                                progress.setValue(progress.getValue()+0.1);
                            }
                            protected void updateProgress(double d){
                                progress.setValue(d);
                            }
                        };
                        Service serviceChoice = new Service<Void>() {
                            @Override
                            protected Task createTask() {
                                return taskChoice;
                            }
                        };
                        serviceChoice.start();

                    } else mon1.setText("Недостаточно средств для покупки");
                } //else mon1.setText("Неверный статус Automata");
        }
        };

        String menuText = atr.printMenu();
        String[] menuTextArr = menuText.split("\n");
        /*for (String i : menuTextArr){
            System.out.println(i);
        }*/
        ArrayList<Button> menuButtonList = new ArrayList<Button>();
        for (int i=0; i<20; i++){
            String buttonText;
            try{
                buttonText = menuTextArr[i];
            }catch (ArrayIndexOutOfBoundsException e){
                buttonText = " ";
            }
            menuButtonList.add(new Button(buttonText));
            if(buttonText != " "){
                menuButtonList.get(i).setId(String.valueOf(i));
                menuButtonList.get(i).setOnAction(menuEvent);
            }else break;
            if(i%2==0){
                leftMenuPanel.getChildren().add(menuButtonList.get(i));
            }else {
                rightMenuPanel.getChildren().add(menuButtonList.get(i));
            }
        }
        /*Button testMenuButton1 = new Button();
        testMenuButton1.setId("1");
        testMenuButton1.setOnAction(menuEvent);
        Button testMenuButton2 = new Button();
        testMenuButton2.setId("2");
        testMenuButton2.setOnAction(menuEvent);*/

        //leftMenuPanel.getChildren().addAll(testMenuButton1,testMenuButton2);

        menuPanel.getChildren().addAll(leftMenuPanel,rightMenuPanel,sepV,coinPanel);
        Separator sepH = new Separator();
        sepH.setOrientation(Orientation.HORIZONTAL);
        VBox root = new VBox();
        root.setSpacing(5);
        root.getChildren().addAll(topPanel,mon1,menuPanel,sepH,pg);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Automata");
        stage.show();
    }

    @Override
    public void run() {
        Application.launch();
    }
}
