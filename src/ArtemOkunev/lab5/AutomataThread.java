/**
 * Created by Artem Okunev on 06.08.2017.
 */
/*
Этот класс должен расширять Automata потоковой функциональностью и механизмами отправки информации
о своём состоянии.
 */
public class AutomataThread extends Automata implements Runnable {
    AutomataGUI ag = null;
    public AutomataThread(){
        super();
    }
    public void setGUIObject(AutomataGUI ag){
        this.ag = ag;
    }
    /*@Override
    public void on(){
        super.on();
    }*/
    @Override
    public void run() {

    }
}
