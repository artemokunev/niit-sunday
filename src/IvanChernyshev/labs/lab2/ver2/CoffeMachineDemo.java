
public class CoffeMachineDemo {
	public static void main(String[] args) {
		CoffeMachine coffe = new CoffeMachine();
		//firstScenario(coffe);
		secondScenario(coffe);
	}

	static void firstScenario(CoffeMachine coffe) {
		coffe.on();
		coffe.coin(15);
		coffe.choise(0);
		coffe.check();
		coffe.coin(15);
		coffe.choise(1);
		coffe.check();
		coffe.cook();
		coffe.finish();
	}

	static void secondScenario(CoffeMachine coffe) {
		coffe.check();
		System.out.println("-----------------------------");
		coffe.on();
		coffe.cancel();
		coffe.choise(2);
		coffe.coin(15);
		coffe.cancel();
		System.out.println("-----------------------------");
		coffe.coin(15);
		coffe.choise(2);
		coffe.check();
		coffe.coin(15);
		coffe.choise(0);
		coffe.check();
		coffe.cook();
		coffe.finish();
		coffe.off();
	}
}
