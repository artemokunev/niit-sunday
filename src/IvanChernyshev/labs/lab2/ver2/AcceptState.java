
public class AcceptState extends States {

	public AcceptState(CoffeMachine coffe) {
		super(coffe);
	}

	@Override
	public void on() {
		resume = "������� ��� �������";
		printResume();
	}

	@Override
	public void off() {
		resume = "������ ��� ��������� �������, ����� ��������� ��������";
		printResume();
	}

	@Override
	public void coin(int cash) {
		resume = "���� ��� ��� ��������";
		printResume();
	}

	@Override
	public void choise() {
		resume = "�������� �������";
		printResume();
		coffe.setCurrentState(coffe.getCheckState());
	}

	@Override
	public void check() {
		resume = "������ ��� ���������� ����� ������� �������";
		printResume();
	}

	@Override
	public void cancel() {
		resume = "�������� ������� " + coffe.getCash() + " ������";
		coffe.setCash(0);
		printResume();
		coffe.setCurrentState(coffe.getWaitState());
	}

	@Override
	public void cook() {
		resume = "������ ��� ����������� �������, ��� ����� �������";
		printResume();
	}

	@Override
	public void finish() {
		resume = "������ ��� ��������� ����� ������";
		printResume();
	}

}
