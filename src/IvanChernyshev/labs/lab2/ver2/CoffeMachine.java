
public class CoffeMachine {
	private States offState;
	private States waitState;
	private States acceptState;
	private States checkState;
	private States cookState;
	private States finishState;
	private States currentState;
	private int cash;
	private int indexOfDrink;
	private String[] menu;
	private int[] prices;

	public CoffeMachine() {
		offState = new OffState(this);
		waitState = new WaitState(this);
		acceptState = new AcceptState(this);
		checkState = new CheckState(this);
		cookState = new CookState(this);
		finishState = new FinishState(this);
		setCurrentState(offState);
		this.menu = new String[] { "������ ����", "�������", "�����" };
		this.prices = new int[] { 20, 30, 35 };
	}

	public void on() {
		currentState.on();
	}

	public void off() {
		currentState.off();
	}

	public void coin(int cash) {
		currentState.coin(cash);
	}

	public void choise(int indexOfDrink) throws IndexOutOfBoundsException {
		setIndexOfDrink(indexOfDrink);
		currentState.choise();
	}

	public void check() {
		currentState.check();
	}

	public void cancel() {
		currentState.cancel();
	}

	public void cook() throws IndexOutOfBoundsException {
		currentState.cook();
	}

	public void finish() {
		currentState.finish();
	}

	public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}

	public String[] getMenu() {
		return menu;
	}

	public int[] getPrices() {
		return prices;
	}

	public States getOffState() {
		return offState;
	}

	public States getWaitState() {
		return waitState;
	}

	public States getAcceptState() {
		return acceptState;
	}

	public States getCheckState() {
		return checkState;
	}

	public States getCookState() {
		return cookState;
	}

	public States getCurrentSrate() {
		return currentState;
	}

	public void setCurrentState(States currentSrate) {
		this.currentState = currentSrate;
	}

	public States getFinishState() {
		return finishState;
	}

	public int getIndexOfDrink() {
		return indexOfDrink;
	}

	public void setIndexOfDrink(int indexOfDrink) {
		this.indexOfDrink = indexOfDrink;
	}
}
