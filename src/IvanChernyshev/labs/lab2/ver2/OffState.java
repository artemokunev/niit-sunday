
public class OffState extends States {

	public OffState(CoffeMachine coffe) {
		super(coffe);
	}

	@Override
	public void on() {
		resume = "������� ����� � �������������";
		printResume();
		for (int i = 0; i < coffe.getMenu().length; i++) {
			System.out.println(coffe.getMenu()[i] + " " + coffe.getPrices()[i]);
		}
		coffe.setCurrentState(coffe.getWaitState());
	}

	@Override
	public void off() {
		resume = "������� ��� ��������, ��� ������� ����� ��������";
		printResume();
	}

	@Override
	public void coin(int cash) {
		resume = "������� ��������, ������ ��� ������ ������ ��� ����� ��������";
		printResume();
	}

	@Override
	public void choise() {
		resume = "������� ��������, ������ ��� ������� ������� ��� ����� ��������";
		printResume();
	}

	@Override
	public void check() {
		resume = "������� ��������, ��� �������� ����������";
		printResume();
	}

	@Override
	public void cancel() {
		resume = "������� ��������, ��� �������� ����������";
		printResume();
	}

	@Override
	public void cook() {
		resume = "������� ��������, ��� �������� ����������";
		printResume();
	}

	@Override
	public void finish() {
		resume = "������� ��������, ��� �������� ����������";
		printResume();
	}
}
