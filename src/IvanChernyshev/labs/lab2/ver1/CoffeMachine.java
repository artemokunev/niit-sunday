
public class CoffeMachine {
	enum States {
		OFF, WAIT, ACCEPT, CHECK, COOK
	};

	private int cash;
	private String[] menu;
	private int[] prices;
	private States state;
	private String resume;

	public CoffeMachine() {
		this.state = States.OFF;
		this.menu = new String[] { "������ ����", "�������", "�����" };
		this.prices = new int[] { 20, 30, 35 };
	}

	public void on() {
		if (state == States.OFF) {
			state = States.WAIT;
			resume = "������� ����� � �������������";
			printResume();
		}
	}

	public void off() {
		if (state == States.WAIT) {
			if (cash > 0) {
				resume = "�������� ������� " + cash + " ������";
				cash = 0;
				printResume();
			}
			state = States.OFF;
			resume = "������� ��������";
			printResume();
		} else {
			resume = "������� ��������� �����";
			printResume();
		}
	}

	public void coin(int cash) {
		if (state == States.WAIT) {
			this.cash += cash;
			this.state = States.ACCEPT;
			resume = "����� ����� �� ����";
			printResume();
		} else {
			resume = "����� ����� �� ���� ������������";
			printResume();
		}
	}

	public void choise(int indexOfDrink) throws IndexOutOfBoundsException {
		if (state == States.ACCEPT) {
			resume = "����� �������";
			printResume();
			check(indexOfDrink);
		} else {
			resume = "����� ������� ������������";
			printResume();
		}
	}

	private void check(int indexOfDrink) {
		state = States.CHECK;
		if (cash == prices[indexOfDrink]) {
			resume = "�������� ������ �������";
			printResume();
			cash = cash - prices[indexOfDrink];
			state = States.COOK;
			cook(indexOfDrink);
		} else if (cash > prices[indexOfDrink]) {
			state = States.COOK;
			cash = cash - prices[indexOfDrink];
			resume = "��� ������� " + (cash);
			printResume();
			cook(indexOfDrink);
		} else if (cash < prices[indexOfDrink]) {
			resume = "���������� ������ " + (prices[indexOfDrink] - cash + " ������");
			printResume();
			state = States.WAIT;
		}
	}

	public void cancel() {
		if (state == States.ACCEPT || state == States.CHECK) {
			this.state = States.WAIT;
			resume = "�������� ������� " + cash + " ������";
			cash = 0;
			printResume();
		} else {
			resume = "��������� � ������� ���� ����� ����� ��������� ��������";
			printResume();
		}
	}

	private void cook(int indexOfDrink) throws IndexOutOfBoundsException {
		if (state == States.COOK) {
			resume = "������������� ������� " + menu[indexOfDrink];
			printResume();
			finish();
		}
	}

	private void finish() {
		this.state = States.WAIT;
		resume = null;
		if (cash > 0) {
			resume = "�������� ������� " + cash + " ������";
			printResume();
			cash = 0;
		}
	}

	public States getState() {
		return state;
	}

	public int getCash() {
		return cash;
	}

	public String[] getMenu() {
		return menu;
	}

	public int[] getPrices() {
		return prices;
	}

	public String getResume() {
		return resume;
	}

	private void printResume() {
		CoffeMachineDemo.printResume(resume);
	}
}
