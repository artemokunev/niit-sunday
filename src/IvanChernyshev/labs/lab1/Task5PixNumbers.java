

import java.util.*;

public class Task5PixNumbers {
	public static void main(String[] args) {
		Task5PixNumbers numbers = new Task5PixNumbers();
		numbers.start();
	}

	public void start() {
		Scanner scanner = new Scanner(System.in);
		printHeader();
		String result = scanner.nextLine();
		parseUserInput(result);
		scanner.close();

	}

	private void parseUserInput(String result) {
		char[] splitted = result.toCharArray();
		int[] splittedNumbers = new int[splitted.length];
		for (int i = 0; i < splitted.length; i++) {
			int res = (int) Character.getNumericValue(splitted[i]);
			splittedNumbers[i] = res;
		}
		printPixResult(splittedNumbers);
	}

	private void printPixResult(int[] result) {
		for (int a = 0; a < 7; a++) {
			for (int i = 0; i < result.length; i++) {
				paintNumber(result[i], a);
			}
			System.out.println();
		}
	}

	private void paintNumber(int c, int layer) {
		if (c == 0) {
			System.out.print(Task5PixNumbersConstants.number0[layer]);
		}
		if (c == 1) {
			System.out.print(Task5PixNumbersConstants.number1[layer]);
		}
		if (c == 2) {
			System.out.print(Task5PixNumbersConstants.number2[layer]);
		}
		if (c == 3) {
			System.out.print(Task5PixNumbersConstants.number3[layer]);
		}
		if (c == 4) {
			System.out.print(Task5PixNumbersConstants.number4[layer]);
		}
		if (c == 5) {
			System.out.print(Task5PixNumbersConstants.number5[layer]);
		}
		if (c == 6) {
			System.out.print(Task5PixNumbersConstants.number6[layer]);
		}
		if (c == 7) {
			System.out.print(Task5PixNumbersConstants.number7[layer]);
		}
		if (c == 8) {
			System.out.print(Task5PixNumbersConstants.number8[layer]);
		}
		if (c == 9) {
			System.out.print(Task5PixNumbersConstants.number9[layer]);
		}

	}

	private void printHeader() {
		System.out.println("������� �����:");
	}
}
