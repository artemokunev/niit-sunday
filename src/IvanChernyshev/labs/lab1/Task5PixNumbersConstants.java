

public class Task5PixNumbersConstants {
	public static final String[] number0 = new String[] { "  ***   ", " *   *  ", "*     * ", "*     * ", "*     * ",
			" *   *  ", "  ***   " };
	public static final String[] number1 = new String[] { "  *   ", " **   ", "  *   ", "  *   ", "  *   ", "  *   ",
			" ***  " };
	public static final String[] number2 = new String[] { " ***  ", "*   * ", "*  *  ", "  *   ", " *    ", "*     ",
			"***** " };
	public static final String[] number3 = new String[] { " ***  ", "*   * ", "    * ", "   *  ", "    * ", "*   * ",
			" ***  " };
	public static final String[] number4 = new String[] { "    *  ", "   **  ", "  * *  ", " *  *  ", "****** ",
			"    *  ", "    *  " };
	public static final String[] number5 = new String[] { "***** ", "*     ", "*     ", "***** ", "    * ", "    * ",
			"***** " };
	public static final String[] number6 = new String[] { "   *  ", "  *   ", " *    ", " ***  ", "*   * ", "*   * ",
			" ***  " };
	public static final String[] number7 = new String[] { "***** ", "   *  ", "  *   ", " *    ", " *    ", " *    ",
			" *    " };
	public static final String[] number8 = new String[] { " ***  ", "*   * ", "*   * ", " ***  ", "*   * ", "*   * ",
			" ***  " };
	public static final String[] number9 = new String[] { " **** ", "*   * ", "*   * ", " **** ", "    * ", "   *  ",
			"  *   " };
}
