
public class Task1Collatz {
	int longestSequence;
	int startNumber;

	public static void main(String[] args) {
		Task1Collatz collatz = new Task1Collatz();
		collatz.calculate();
	}

	public void calculate() {
		for (int i = 1; i < 100000; i++) {
			calculate(i);
		}
		printLongestSequence();
	}

	private void calculate(int index) {
		int localNumber = index;
		int numberOfIterations = 0;
		do {
			if (isOddNumber(index)) {
				index = index * 3 + 1;
			} else {
				index = index / 2;
			}
			numberOfIterations++;
		} while (index != 1);
		// System.out.println(numberOfIterations + " последовательностей для
		// числа " + localNumber);
		compareOfResults(localNumber, numberOfIterations);
	}

	private void compareOfResults(int index, int iterations) {
		if (iterations > longestSequence) {
			this.startNumber = index;
			this.longestSequence = iterations;
		}
	}

	private void printLongestSequence() {
		System.out.println(longestSequence + " последовательностей для числа " + startNumber);
	}

	private boolean isOddNumber(int index) {
		return index % 2 != 0 ? true : false;
	}
}
