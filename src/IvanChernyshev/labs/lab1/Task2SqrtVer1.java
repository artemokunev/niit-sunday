

public class Task2SqrtVer1 {
	final double delta = 0.00000001;

	double average(double x, double y) {
		return (x + y) / 2.0;
	}

	boolean good(double guess, double x) {
		return Math.abs(guess * guess - x) < delta;
	}

	double improve(double guess, double x) {
		return average(guess, x / guess);
	}

	double iter(double guess, double x) {
		if (good(guess, x)) {
			return guess;
		} else {
			return iter(improve(guess, x), x);
		}
	}

	public double calc(double arg) {
		return iter(1.0, arg);
	}
}
