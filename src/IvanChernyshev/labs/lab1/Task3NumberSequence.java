

import java.util.*;

public class Task3NumberSequence {
	ArrayList<Integer> result;

	public static void main(String[] args) {
		new Task3NumberSequence().start();
	}

	public void start() {
		Scanner scanner = new Scanner(System.in);
		printHeader();
		String result = scanner.nextLine();
		scanner.close();
		parseUserInput(result);
	}

	private void parseUserInput(String res) {
		String[] results = res.split(",");
		parseSequence(results);
	}

	private void parseSequence(String[] results) {
		ArrayList<String> result = new ArrayList<String>();
		String[] sequence = null;
		for (String string : results) {
			sequence = string.split("-");
			for (String st : sequence)
				result.add(st);
		}
		try {
			toIntList(result);
			addLostNumbers();
			printResult();
		} catch (UnsupportedOperationException ex) {
			printWarning();
		}
	}

	private void toIntList(ArrayList<String> res) {
		result = new ArrayList<Integer>();
		try {
			for (String st : res) {
				int r = (int) Integer.parseInt(st);
				result.add(r);
			}
		} catch (NumberFormatException ex) {
			throw new UnsupportedOperationException();
		}
	}

	private void addLostNumbers() {
		int valOfFirst = result.get(0);
		int indexOfFirst = 0;
		for (int index = 0; index < result.size(); index++) {
			if (valOfFirst < result.get(index)) {
				result.add(indexOfFirst, valOfFirst);
			}
			valOfFirst++;
			indexOfFirst++;
		}
	}

	private void printHeader() {
		System.out.println("������� ������������������ �����:");
	}

	private void printResult() {
		for (int res : result) {
			System.out.print(res + " ");
		}
		System.out.println(result.toString());
	}

	private void printWarning() {
		System.out.println("����� ������� ������ ��������� ��������, ���������� ��� ���!");
	}

}
