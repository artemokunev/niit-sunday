
import java.util.*;

public class Student implements Comparable<Student> {
	private int ID;
	private String fio;
	private Group group;
	private ArrayList<Integer> marks;
	private int num;

	public Student(int ID, String fio) {
		this.ID = ID;
		this.fio = fio;
		this.marks = new ArrayList<Integer>();
	}

	public int getID() {
		return ID;
	}

	public String getFio() {
		return fio;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public ArrayList<Integer> getMarks() {
		return marks;
	}

	public int getNum() {
		return num;
	}

	private void numberOfNum() {
		this.num = marks.size();
	}

	public void addMark(int mark) {
		this.marks.add(mark);
		numberOfNum();
	}

	public double averageMarkCalc() {
		double averageMark = 0;
		for (int mark : marks) {
			averageMark = averageMark + mark;
		}
		return averageMark / marks.size();
	}

	@Override
	public String toString() {
		return this.ID + " " + this.fio;
	}

	@Override
	public int compareTo(Student student) {
		return this.getFio().compareTo(student.getFio());
	}

	public void printInfo() {
		System.out.println(this.toString());
		System.out.println("������ \n" + getMarks());
		System.out.println("������� ������ \n" + String.format("%.2f", averageMarkCalc()) + "\n");
	}
}
