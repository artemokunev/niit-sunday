package moon;

import java.awt.*;
import java.awt.event.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.LocalTime;

public class MoonClock extends JComponent implements MouseMotionListener, Runnable
{
    private int smaller=3;
    private static Stroke SEC_STROKE = new BasicStroke(4F);
    private static Stroke MIN_STROKE =
            new BasicStroke(16F, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND);
    private static Stroke HOUR_STROKE = MIN_STROKE;
    private static Stroke BACK_STROKE = new BasicStroke(4F, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL); // для бэкграунда

    private Point mp;
    private JFrame parent;

    private Dimension size = null;
    private long wcounter = 0;
    private Indicators ind = new Indicators(); // индикаторы
    private final String DEGREE  = "\u00b0"; // градус

    private MoonClock(final JFrame parent)  {
        (new Thread(this)).start();
        this.parent = parent;
        addMouseMotionListener(this);
        addMouseListener(new MouseAdapter()
        {
            public void mousePressed(MouseEvent e) // ловим нажатие
            {
                mp = e.getPoint();
                getComponentAt(mp);
            }

            public void mouseClicked(MouseEvent e) { // ловим двойной клик
                if (e.getClickCount() == 2) {
                    System.exit(0);
                }
            }
        });
    }

    public void run() {
        try {
            while (true) {
                Thread.sleep(500);
                repaint();
                if ((wcounter==0) || (wcounter==2*60*30)) { // каждые полчаса получать погоду
                    ind.getWeather();
                    wcounter=1;
                }
                else wcounter++;
            }
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void drawBackground(Graphics2D g2d, int radius) {

        BufferedImage background;
        try {
            background = ImageIO.read(this.getClass().getResource("/images/stars1.png")); // бэкграунд
            Image scaledback = background.getScaledInstance(2*radius, 2*radius, Image.SCALE_DEFAULT);
            g2d.drawImage(scaledback, -radius, -radius, this);
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        g2d.setStroke(BACK_STROKE);
        g2d.setColor(Color.black);
        g2d.drawOval(-radius/(smaller*2)-radius/2,-radius/(smaller*2), radius/smaller, radius/smaller); // кружок для индикатора
        g2d.drawOval(-radius/(smaller*2)+radius/2,-radius/(smaller*2), radius/smaller, radius/smaller); // кружок для индикатора
        g2d.drawOval(-radius/(smaller*2),-radius/(smaller*2)-radius/2, radius/smaller, radius/smaller); // кружок для индикатора

        g2d.setColor(Color.white);
        g2d.drawOval(-radius/(smaller*2)-radius/2-4,-radius/(smaller*2)-4, radius/smaller+8, radius/smaller+8); // внутренний кружочек
        g2d.drawOval(-radius/(smaller*2)+radius/2-4, -radius/(smaller*2)-4, radius/smaller+8, radius/smaller+8); // внутренний кружочек

        Arc2D arcLeft = new Arc2D.Double(-radius/(smaller),-radius/(smaller*2)+radius/2, radius/smaller, radius/smaller, 90, 180, Arc2D.OPEN);
        Arc2D arcRight = new Arc2D.Double(0,-radius/(smaller*2)+radius/2, radius/smaller, radius/smaller, 270, 180, Arc2D.OPEN);
        g2d.setColor(Color.white);
        g2d.draw(arcLeft); // полукруг слева
        g2d.draw(arcRight); // полукруг справаё
        g2d.drawLine(radius/(smaller*2), -radius/(smaller*2)+radius/2, -radius/(smaller*2),-radius/(smaller*2)+radius/2);
        g2d.drawLine(radius/(smaller*2), radius/(smaller*2)+radius/2+1, -radius/(smaller*2),radius/(smaller*2)+radius/2+1);

    }

    private void drawIndicators(Graphics2D g2d, int radius) {

        try {
            Font Merkur = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/fonts/Merkur.ttf")); // подгружакем шрифт
            g2d.setColor(Color.yellow);
            g2d.setFont(Merkur.deriveFont(Font.PLAIN, radius/7));
            g2d.drawString(ind.getDate(), -radius/(smaller)+radius/(smaller*8),radius/2);   // напишем дату
            g2d.drawString(ind.getWeekDay(), -radius/(smaller*4),radius/2+3*radius/(smaller*8));  // напишем день недели
        }
        catch(FontFormatException | IOException ex) {
            System.out.println(ex.getMessage());
        }

        g2d.setColor(Color.yellow);
        int startAngle = (int)(ind.getCPU()*360);
        g2d.fillArc(-radius/(smaller*2)-radius/2,-radius/(smaller*2), radius/smaller, radius/smaller, 90, -startAngle); // рисуем Pie отображающий загрузку

        g2d.setColor(Color.red);
        g2d.drawString("CPU", -3*radius/(smaller*8)-radius/2,radius/(smaller*16)+8); // просто текст

        int d = ind.getMoonPhase(); // фаза луны (от 0 до 27)
        final int moon_w=261; // ширина картинки
        final int moon_h=361; // высота картинки

        BufferedImage moon;
        try {
            moon = ImageIO.read(this.getClass().getResource("/images/MoonPhasesT.png")); // подгружаем картинку
            int moon_sm_width = (int)Math.round(radius/smaller*((double)moon_w/moon_h)); // подгоним ширину
            Image scaledMoon = moon.getSubimage(moon_w*d,0,moon_w,moon_h).getScaledInstance(moon_sm_width, radius/smaller, Image.SCALE_SMOOTH);
            g2d.drawImage(scaledMoon, -moon_sm_width/2,-radius/(smaller*2)-radius/2, this);
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        try {
            Font BlackOpsOne = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/fonts/BlackOpsOne.ttf"));
            g2d.setFont(BlackOpsOne.deriveFont(Font.PLAIN, radius/11));
            g2d.setColor(Color.white);
            g2d.drawString(String.valueOf(ind.getTemperature()) + DEGREE, radius/2-radius/(smaller*4),-radius/(smaller*16)); // выводим температуру
            g2d.drawString(String.valueOf(ind.getHumidity()) + "%", radius/2-radius/(smaller*4),radius/(smaller*4)+radius/(smaller*16)); // выводим влажность
        }
        catch(FontFormatException | IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void drawHands(Graphics2D g2d, int radius) {
        LocalTime lt=LocalTime.now();

        long minute=lt.getMinute();
        long sec = lt.getSecond();
        long hour = lt.getHour();

        double sec_angle = -Math.PI/2+sec*6.0*Math.PI/180;
        double min_angle = -Math.PI/2+6.0*Math.PI/180*
                (minute+sec/60.0);
        double hour_angle = -Math.PI/2+30.0*Math.PI/180*
                (hour+minute/60.0);

        int cutter = radius / 5;
        int hwidth = radius / 20;

        // draw the hours
        g2d.setColor(Color.red);
        g2d.setStroke(HOUR_STROKE);
        g2d.rotate(hour_angle);
        g2d.drawLine(0, -hwidth, radius - cutter*3, -hwidth);
        g2d.drawLine(0, hwidth, radius - cutter*3, hwidth);
        g2d.drawLine(radius-cutter*3, -hwidth, radius - cutter*3 + hwidth, 0);
        g2d.drawLine(radius-cutter*3, hwidth, radius - cutter*3 + hwidth, 0);
        g2d.rotate(-hour_angle);

        // draw the minutes
        g2d.setColor(Color.lightGray);
        g2d.setStroke(MIN_STROKE);
        g2d.rotate(min_angle);
        g2d.drawLine(0, -hwidth, radius - cutter*2, -hwidth);
        g2d.drawLine(0, hwidth, radius - cutter*2, hwidth);
        g2d.drawLine(radius-cutter*2, -hwidth, radius - cutter*2 + hwidth, 0);
        g2d.drawLine(radius-cutter*2, hwidth, radius - cutter*2 + hwidth, 0);
        g2d.rotate(-min_angle);

        // draw the seconds
        g2d.setColor(Color.BLUE);
        g2d.setStroke(SEC_STROKE);
        g2d.rotate(sec_angle);
        g2d.drawLine(0, -hwidth/2, radius - cutter, -hwidth/2);
        g2d.drawLine(0, hwidth/2, radius - cutter, hwidth/2);
        g2d.drawLine(radius-cutter, -hwidth/2, radius - cutter + hwidth, 0);
        g2d.drawLine(radius-cutter, hwidth/2, radius - cutter + hwidth, 0);
        g2d.rotate(-sec_angle);

        g2d.setColor(Color.black);
        g2d.setStroke(HOUR_STROKE);
        g2d.fillOval(-hwidth*2, -hwidth*2, hwidth*4, hwidth*4);
    }

    private void drawPerimeter(Graphics2D g2d, int radius) {
        // draw the perimeter
        g2d.setColor(Color.white);
        g2d.drawOval(-radius, -radius , 2 * radius, 2 * radius);

        for (int j = 0; j < 60; ++j) {
            if ((j % 5) == 0) {
                g2d.drawLine(radius - 20, 0, radius, 0);
            }
            else
                g2d.drawLine(radius-10, 0, radius, 0);
            g2d.rotate(6.0*Math.PI/180);
        }

        try {
            Font BlackOpsOne = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/fonts/BlackOpsOne.ttf"));
            g2d.setFont(BlackOpsOne.deriveFont(Font.PLAIN, radius/5));
            g2d.setColor(Color.green);
            g2d.drawString("0", -radius/16, -radius+radius/(smaller*4)+radius/8); // циферки по полюсам часов
            g2d.drawString("3", radius-radius/(smaller*4)-radius/8, radius/16);
            g2d.drawString("6", -radius/16, radius-radius/(smaller*4));
            g2d.drawString("9", -radius+radius/(smaller*4), radius/16);
        }
        catch(FontFormatException | IOException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void paint(Graphics graphics) {
        super.paint(graphics);

        // Draw the hands
        Graphics2D g = (Graphics2D) graphics;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        size = getSize(size);
        //insets = getInsets(insets);
        int radius =
                Math.min(size.width,size.height) / 2;
        g.translate((double)size.width/2D,(double)size.height/2D);

        drawBackground(g, radius);
        drawIndicators(g, radius);
        drawPerimeter(g, radius);
        drawHands(g, radius);

    }

    @Override
    public void mouseDragged(MouseEvent e) {

        // get location of Window
        int thisX = parent.getLocation().x;
        int thisY = parent.getLocation().y;

        // Determine how much the mouse moved since the initial click
        int xMoved = (thisX + e.getX()) - (thisX + mp.x);
        int yMoved = (thisY + e.getY()) - (thisY + mp.y);

        // Move window to this position
        int X = thisX + xMoved;
        int Y = thisY + yMoved;
        parent.setLocation(X, Y);
    }

    @Override
    public void mouseMoved(MouseEvent m) {
        mp = m.getPoint();
    }

    public static void main(String[] args)
    {
        final int size = 600;  // размер

        JFrame f = new JFrame("Moon Clock");
        MoonClock clock = new MoonClock(f);
        f.add(clock);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        f.setUndecorated(true);
        f.setShape(new Ellipse2D.Double(0, 0, size, size)); // сделаем фрейм в виде круга
        f.setSize(size, size);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
        f.setLocation(new Point((int)Math.round(width/2 - size/2), (int)Math.round(height/2 - size/2))); // поместим в центр экрана
        f.setVisible(true);
    }

}

