package moon;

import com.sun.management.OperatingSystemMXBean;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.Format;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

class Indicators { // класс для расчета индикаторов выводимых в часах

    private int temperature = 0;
    private int humidity = 0;

    public int getTemperature() { // температура
        return temperature;
    }

    public int getHumidity() { // влажность
        return humidity;
    }

    public double getCPU() { // загрузка процессора
        OperatingSystemMXBean operatingSystemMXBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        return operatingSystemMXBean.getProcessCpuLoad();
    }

    public int getMoonPhase() { // фаза луны
        double y;
        int m = LocalDate.now().getMonthValue();
        int d = LocalDate.now().getDayOfMonth();
        if(m<3) y=(LocalDate.now().getYear()-1)/19;
        else y=LocalDate.now().getYear()/19;
        y = Math.round((y - (int)y) * 209);
        if(m<3) y+=12+m-3;
        else y+=m-3;
        y=(y+d)/30;
        y = Math.round((y - (int)y) * 30);
        if(y>27)y=27;
        return (int)Math.round(y);
    }

    public void getWeather() { // получить текущий прогноз погоды
        BufferedReader reader = null;
        int temp=0;
        int humd = 0;
        try {
            JSONParser parser = new JSONParser(); // с сервера yahoo
            URL url = new URL("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22Nizhniy%20Novgorod%2C%20ru%22)%20and%20u%3D'c'&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=");
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            JSONObject js = (JSONObject)parser.parse(reader);
            JSONObject queryObj = (JSONObject)js.get("query");
            JSONObject resObj = (JSONObject)queryObj.get("results");
            JSONObject chnlObj = (JSONObject)resObj.get("channel");
            JSONObject atmoObj = (JSONObject)chnlObj.get("atmosphere");
            JSONObject itemObj = (JSONObject)chnlObj.get("item");
            JSONObject condObj = (JSONObject)itemObj.get("condition");
            temp = Integer.parseInt((condObj.get("temp")).toString());
            humd = Integer.parseInt((atmoObj.get("humidity")).toString());
        }
        catch(IOException | ParseException ex) {
            System.out.println(ex.getMessage());
        }
        temperature = temp;
        humidity = humd;
    }

    public String getDate() { // текущая дата
        LocalDate date = LocalDate.now();
        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.uuuu");
        return date.format(df);
    }

    public String getWeekDay() { // текущий день недели
        LocalDate date = LocalDate.now();
        DateTimeFormatter df = DateTimeFormatter.ofPattern("eee");
        return date.format(df);
    }
}
