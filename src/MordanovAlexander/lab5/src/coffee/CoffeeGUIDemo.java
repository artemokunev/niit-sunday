package coffee;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class CoffeeGUIDemo extends Application {

    private Stage primaryStage;
    private GridPane rootLayout;

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Coffee Automat");
        showRootLayout();
    }

    private void showRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            //loader.setLocation(CoffeeGUIDemo.class.getResource("views/automata.fxml"));
            loader.setLocation(this.getClass().getResource("/views/automata.fxml"));
            rootLayout = loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }


    public static void main(String[] args) {
        launch(args);
    }
}
