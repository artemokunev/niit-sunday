package coffee;

import com.sun.jnlp.ApiDialog;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import my.CoffeeOutputer;
import my.except.CoffeeException;
import my.templates.AbstractCoffeeOutputer;
import my.CoffeeAutomat;

import static java.lang.Thread.sleep;

public class Controller implements Initializable {

    //-----------------кнопки интерфейса выбора напитка и добавки (середина)
    @FXML private Button btnAdd0; // кнопка добавления добавки №1
    @FXML private Button btnAdd1; // кнопка добавления добавки №2
    @FXML private Button btnAdd2; // кнопка добавления добавки №3
    @FXML private Button btnAdd3; // кнопка добавления добавки №4
    @FXML private List<Button> addButtons = new ArrayList<Button>() {};
    @FXML protected void btnAdd0Action(ActionEvent e) {
        coffee.choice(coffee.getAddsMenu()[0].getIndex());
    }
    @FXML protected void btnAdd1Action(ActionEvent e) {
        coffee.choice(coffee.getAddsMenu()[1].getIndex());
    }
    @FXML protected void btnAdd2Action(ActionEvent e) {
        coffee.choice(coffee.getAddsMenu()[2].getIndex());
    }
    @FXML protected void btnAdd3Action(ActionEvent e) {
        coffee.choice(coffee.getAddsMenu()[3].getIndex());
    }

    @FXML private Button btnDrink0; // кнопка добавления напитка №1
    @FXML private Button btnDrink1; // кнопка добавления напитка №2
    @FXML private Button btnDrink2; // кнопка добавления напитка №3
    @FXML private Button btnDrink3; // кнопка добавления напитка №4
    @FXML private Button btnDrink4; // кнопка добавления напитка №5
    @FXML private Button btnDrink5; // кнопка добавления напитка №6
    @FXML private Button btnDrink6; // кнопка добавления напитка №7
    @FXML private Button btnDrink7; // кнопка добавления напитка №8
    @FXML private List<Button> drinkButtons = new ArrayList<Button>() {};
    @FXML protected void btnDrink0Action(ActionEvent e) {
        coffee.choice(coffee.getDrinksMenu()[0].getIndex());
    }
    @FXML protected void btnDrink1Action(ActionEvent e) {
        coffee.choice(coffee.getDrinksMenu()[1].getIndex());
    }
    @FXML protected void btnDrink2Action(ActionEvent e) {
        coffee.choice(coffee.getDrinksMenu()[2].getIndex());
    }
    @FXML protected void btnDrink3Action(ActionEvent e) {
        coffee.choice(coffee.getDrinksMenu()[3].getIndex());
    }
    @FXML protected void btnDrink4Action(ActionEvent e) {
        coffee.choice(coffee.getDrinksMenu()[4].getIndex());
    }
    @FXML protected void btnDrink5Action(ActionEvent e) {
        coffee.choice(coffee.getDrinksMenu()[5].getIndex());
    }
    @FXML protected void btnDrink6Action(ActionEvent e) {
        coffee.choice(coffee.getDrinksMenu()[6].getIndex());
    }
    @FXML protected void btnDrink7Action(ActionEvent e) {
        coffee.choice(coffee.getDrinksMenu()[7].getIndex());
    }

    //-----------------сдача (левый нижний угол)
    @FXML private Button btnClearChange;
    @FXML private TextField textChange;
    @FXML protected void clearChange(ActionEvent event) {
        textChange.setText("0");
        image.setImage(null);
        coffee.clearOutputer();
    }

    //----------------кнопки монеток (левый верхний угол)
    @FXML private Button btnCoin1;  // кнопка монетки 1р.
    @FXML private Button btnCoin2;  // кнопка монетки 2р.
    @FXML private Button btnCoin5;  // кнопка монетки 5р.
    @FXML private Button btnCoin10; // кнопка монетки 10р.
    @FXML private Button btnCoin50; // кнопка монетки 50р.
    @FXML private List<Button> coinButtons = new ArrayList<Button>() {};
    @FXML protected void btnCoin1Action(ActionEvent e) {
        coffee.coin(1);
        cash.setText(coffee.getCash());
        cashSum.setText(String.valueOf(coffee.getCashSum()));
    }
    @FXML protected void btnCoin2Action(ActionEvent e) {
        coffee.coin(2);
        cash.setText(coffee.getCash());
        cashSum.setText(String.valueOf(coffee.getCashSum()));
    }
    @FXML protected void btnCoin5Action(ActionEvent e) {
        coffee.coin(5);
        cash.setText(coffee.getCash());
        cashSum.setText(String.valueOf(coffee.getCashSum()));
    }
    @FXML protected void btnCoin10Action(ActionEvent e) {
        coffee.coin(10);
        cash.setText(coffee.getCash());
        cashSum.setText(String.valueOf(coffee.getCashSum()));
    }
    @FXML protected void btnCoin50Action(ActionEvent e) {
        coffee.coin(50);
        cash.setText(coffee.getCash());
        cashSum.setText(String.valueOf(coffee.getCashSum()));
    }

    //----------------экран вывода (сверху в середине)
    @FXML private TextArea outputScreen;

    //----------------текущий монетоприемник (сверху справа)
    @FXML private TextField cash;
    @FXML private TextField cashSum;
    @FXML private Button cancelButton;
    @FXML protected void cancelButtonAction(ActionEvent e) {
        textChange.setText(String.valueOf(coffee.getCashSum()));
        coffee.clearChange();
        cash.setText(coffee.getCash());
        cashSum.setText(String.valueOf(coffee.getCashSum()));
    }
    @FXML private Button btnOn; // кнопка "ВКЛ автомат"
    @FXML private Button btnOff; // кнопка "ВЫКЛ автомат"
    @FXML protected void btnOnAction(ActionEvent e) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Подтверждение");
        alert.setHeaderText("ВКЛЮЧЕНИЕ АВТОМАТА!");
        alert.setContentText("Вы действительно хотите включить автомат?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            btnOn.setDisable(true);
            btnOff.setDisable(false);
            coffee.on();
        }
    }
    @FXML protected void btnOffAction(ActionEvent e) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Подтверждение");
        alert.setHeaderText("ВЫКЛЮЧЕНИЕ АВТОМАТА!");
        alert.setContentText("Вы действительно хотите выключить автомат?");
        alert.getButtonTypes().add(ButtonType.CANCEL);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            btnOn.setDisable(false);
            btnOff.setDisable(true);
            coffee.off();
        }
    }

    //----------------журнал событий (справа посередине)
    @FXML private ListView innerlog;

    //----------------внутренние счетчики монет (снизу справа)
    @FXML private TextField counter1;  // счетчик монеток 1р.
    @FXML private TextField counter2;  // счетчик монеток 2р.
    @FXML private TextField counter5;  // счетчик монеток 5р.
    @FXML private TextField counter10; // счетчик монеток 10р.
    @FXML private TextField counter50; // счетчик монеток 50р.
    @FXML private List<TextField> counters = new ArrayList<TextField>() {};
    @FXML private TextField counterSum; // счетчик суммы монеток

    //----------------кнопка "Приготовить!"
    @FXML private Button cookButton;
    @FXML protected void cookButtonAction(ActionEvent e) {

        if(!coffee.check()) return; // проверяем на достаточность денег

        if(!coffee.cook()) return; // готовим

        for (Button b : drinkButtons) b.setDisable(true);
        for (Button b : addButtons) b.setDisable(true);
        for (Button b : coinButtons) b.setDisable(true);
        Image img = new Image(this.getClass().getResource("/images/java-coffee_empty.png").toString());
        image.setImage(img);

        new Thread() {

            // runnable for that thread
            @Override
            public void run() {
                for (int i = 0; i < 20; i++) {
                    try {
                        // imitating work
                        Thread.sleep(150);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    final double progress = i*0.05;
                    // update ProgressIndicator on FX thread
                    Platform.runLater(new Runnable() {

                        public void run() {
                            pbar.setProgress(progress);
                        }
                    });
                }
                Platform.runLater(() -> {
                    Image img1 = new Image(this.getClass().getResource("/images/java-coffee.png").toString());
                    image.setImage(img1);
                    pbar.setProgress(0);
                    for (Button b : drinkButtons) b.setDisable(false);
                    for (Button b : addButtons) b.setDisable(false);
                    for (Button b : coinButtons) b.setDisable(false);

                    textChange.setText(String.valueOf(coffee.getlastChange()));
                    cash.setText(coffee.getCash());
                    cashSum.setText(String.valueOf(coffee.getCashSum()));
                    for(int i=0;i<5;i++) {counters.get(i).setText(String.valueOf(coffee.getCounters()[i])); }
                    counterSum.setText(String.valueOf(coffee.getCountersSum()));

                    coffee.finish();
                });
            }
        }.start();

    }

    //----------------картинка для чашки с кофе
    @FXML private ImageView image;
    @FXML private ProgressBar pbar;

    private AbstractCoffeeOutputer outputer;
    private CoffeeAutomat coffee;

    @Override @FXML
    public void initialize(URL url, ResourceBundle rb) {
        try {
            outputer = new CoffeeOutputer(innerlog, outputScreen);
            coffee = new CoffeeAutomat(this.getClass().getResource("/views/menu.txt").toString(), outputer); // автомат, получает список меню из файла, получает "выводитель" информации
            coffee.on();
        }
        catch(CoffeeException ex) {
            return;
        }
        drinkButtons.add(btnDrink0);
        drinkButtons.add(btnDrink1);
        drinkButtons.add(btnDrink2);
        drinkButtons.add(btnDrink3);
        drinkButtons.add(btnDrink4);
        drinkButtons.add(btnDrink5);
        drinkButtons.add(btnDrink6);
        drinkButtons.add(btnDrink7);

        addButtons.add(btnAdd0);
        addButtons.add(btnAdd1);
        addButtons.add(btnAdd2);
        addButtons.add(btnAdd3);

        counters.add(counter1);
        counters.add(counter2);
        counters.add(counter5);
        counters.add(counter10);
        counters.add(counter50);

        coinButtons.add(btnCoin1);
        coinButtons.add(btnCoin2);
        coinButtons.add(btnCoin5);
        coinButtons.add(btnCoin10);
        coinButtons.add(btnCoin50);

        if(coffee.getDrinksMenu().length>=8) {
            for (int i = 0; i < drinkButtons.size(); i++)
                drinkButtons.get(i).setText(coffee.getDrinksMenu()[i].getName() + " (" + coffee.getDrinksMenu()[i].getPrice() + ")");
        }
        if(coffee.getDrinksMenu().length>=4) {
            for (int i = 0; i < addButtons.size(); i++)
                addButtons.get(i).setText(coffee.getAddsMenu()[i].getName() + " (" + coffee.getAddsMenu()[i].getPrice() + ")");
        }
    }

}

