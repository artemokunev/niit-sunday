package my;

public class MenuItem { // описание одного элемента меню
    protected int index; // номер по порядку
    protected boolean type; // тип напитка (true - это напиток, false - добавка)
    protected String name; // название
    protected int parameter; // параметр - для напитка можно ли добавлять добавки, для добавки - максимальное количество
    protected long price; // цена

    public MenuItem(int index, boolean type, String name, int parameter, long price) {
        this.index = index;
        this.type = type;
        this.name = name;
        this.parameter = parameter;
        this.price = price;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public boolean gettype() {
        return type;
    }

    public long getPrice() {
        return price;
    }

    public int getParameter() {
        return parameter;
    }
}
