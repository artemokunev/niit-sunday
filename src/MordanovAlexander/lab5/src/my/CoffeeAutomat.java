package my;

import my.except.*;
import my.templates.AbstractCoffeeAutomat;
import my.templates.AbstractCoffeeOutputer;
import my.templates.CoffeeDrink;
import my.templates.CoffeeMoneyKeeper;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CoffeeAutomat implements AbstractCoffeeAutomat {

    private AbstractCoffeeOutputer outputer; // класс для вывода ошибок и результата работы
    private CoffeeMoneyKeeper moneykeeper = new CoffeeMoneyKeeper(); // хранилище денег
    private CoffeeDrink drink = new CoffeeDrink(); // хранилище напитка

    public boolean on() { // эмуляция включения автомата
        this.currentState = states.WAIT;
        try {
            loadMenu();
            this.outputer.write2log("Автомат запущен, меню загружено, ожидание...");
            this.outputer.showLine("Выберите напиток");
        }
        catch(CoffeeException ex) {
            this.outputer.write2log("Ошибка загрузки меню! " + ex.errorState + ", " + ex.errorParameter + ", " + ex.getMessage());
            return false;
        }
        outputer.clearOutput();
        return true;
    }

    public void clearOutputer() {
        this.outputer.clearOutput();
    }

    public boolean off() { // эмуляция выключения автомата
        outputer.showLine("Выключение автомата");
        if(moneykeeper.getKeeperSum()>0) {
            outputer.showLine("В счетчике монет " + moneykeeper.getKeeperSum() + " монет на " + moneykeeper.getKeeperCount() + " р.");
            outputer.write2log("Из счетчика в хранилище: " + moneykeeper.getKeeperSum() + " монет на " + moneykeeper.getKeeperCount() + " р.");
            moneykeeper.flushKeeper();
        }
        this.currentState = states.OFF;
        outputer.write2log("Автомат выключается...");
        for(int i=0;i<moneykeeper.getCounters().length;i++) {
            outputer.write2log("В хранилище №" + i + " (номинал: " + moneykeeper.validcoins[i] + ") - " + moneykeeper.getCounters()[i] + " монет на " + moneykeeper.validcoins[i]*moneykeeper.getCounters()[i]);
        }
        outputer.write2log("Итого в хранилище: " + moneykeeper.getCountersSum() + " р.");
        outputer.write2log("Автомат выключен.");
        try {
            outputer.stop();
        } catch (CoffeeException ex) {
            this.outputer.write2log("Ошибка закрытия логирователя! " + ex.errorState + ", " + ex.errorParameter + ", " + ex.getMessage());
        }
        outputer.clearOutput();
        return true;
    }

    public boolean coin(int coin) { // эмуляция внесения денег (проверка на правильность)
        if(currentState!=states.WAIT) return false;
        currentState = states.ACCEPT;
        if(moneykeeper.checkCoin(coin)) { // проверяем на валидность монетку
            moneykeeper.insertCoin(coin);
            outputer.showLine("Внесена монета в " + coin + " р. (всего: " + moneykeeper.getKeeperSum() + " р.)");
            outputer.write2log("Монета: " + coin);
        }
        else {
            outputer.write2log("Попала неверная монета: " + coin + " р.");
            outputer.showLine("Неверный номинал монеты!");
        }
        currentState = states.WAIT;
        return true;
    }

    public boolean choice(int index) { // эмуляция выбора напитка + проверка заказа (на добавки)
        if(currentState!=states.WAIT) return false;
        currentState = states.ACCEPT;
        MenuItem mi = null;
        for(MenuItem mi0:menulist) {
            if(mi0.index==index) {
                mi = mi0; // получим описание напитка
                break;
            }
        }
        if(mi==null) {
            currentState = states.ERROR;
            outputer.write2log("Неизвестный напиток с индексом: " + index);
            return false;
        }
        if(mi.type) {
            // это напиток
            drink.setDrink(mi.index, mi.parameter!=0, mi.price, mi.name);
            outputer.write2log("Выбран напиток: " + mi.index + ", " + mi.price);
            outputer.showLine("Напиток " + mi.name + " : " + mi.price + " р.");
        }
        else {
            if(drink.getDrink()==-1) {
                outputer.write2log("Попытка добавить " + mi.name + " без выбора напитка");
                outputer.showLine("Сначала выберите напиток!");
            }
            else
            switch(drink.setAdd(mi.index, 1, mi.price, mi.parameter)) {
                case -1: // нельзя добавлять в напиток ничего
                    outputer.write2log("Попытка добавить " + mi.name + " в " + menulist.get(drink.getDrink()).name);
                    outputer.showLine("В напиток " + drink.name + " нельзя добавить добавки!");
                    break;
                case 1: // нельзя добавить - уже максимальное количество добавок
                    outputer.write2log("Добавка: " + mi.index + ", " + mi.price);
                    outputer.showLine("Нельзя добавить " + mi.name + ", макс. количество - " + mi.parameter);
                    break;
                case 2: // добавили новую добавку
                    outputer.write2log("Добавка: " + mi.index + ", " + mi.price);
                    outputer.showLine("+ " + mi.name + " : " + mi.price + " р., сумма: " + drink.getPrice());
                    break;
                default: // добавка была, добавили еще
                    outputer.write2log("Добавка: " + mi.index + ", " + mi.price + ", всего: " + drink.getAddsCount());
                    outputer.showLine("+ " + mi.name + " : " + mi.price + " р., всего: " + drink.getAddsCount() + ", сумма: " + drink.getPrice());
            }
        }
        currentState = states.WAIT;
        return true;
    }

    public boolean check() { // эмуляция проверки заказа (стоимость)
        if(currentState!=states.WAIT) return false;
        if(drink.getDrink()==-1) {
            outputer.showLine("Выберите напиток!");
            outputer.write2log("Не был выбран напиток!");
            return false;
        }
        currentState = states.CHECK;
        outputer.write2log("Проверка заказа на сумму " + drink.getPrice());
        if(moneykeeper.getKeeperSum()>=drink.getPrice()) {
            outputer.showLine("Готовим напиток " + drink.name + "...");
        }
        else {
            outputer.write2log("Попытка купить напиток (" + drink.getPrice() + ") за " + moneykeeper.getKeeperSum() + " р.");
            outputer.showLine("Не хватает средств! Выберите другой напиток или внесите еще монеты.");
            currentState = states.WAIT;
        }
        return true;
    }

    public boolean cancel() { // эмуляция отмены заказа
        if(currentState!=states.WAIT) return false;
        outputer.showLine("Отмена! Возьмите сдачу " + moneykeeper.getKeeperSum() + " р.");
        odd(false);
        outputer.clearOutput();
        return true;
    }

    public boolean cook() { // эмуляция приготовления заказа
        long keepersum = moneykeeper.getKeeperSum();
        if(currentState!=states.CHECK) return false;
        currentState = states.COOK;
        moneykeeper.flushKeeper();
        if(keepersum>drink.getPrice()) { // нужно сдавать сдачу
            if(moneykeeper.odd(keepersum-drink.getPrice())) {
                outputer.showLine("Возьмите сдачу: " + (keepersum-drink.getPrice()) + " р.");
                outputer.write2log("Сдача " + (keepersum-drink.getPrice()) + " р.");
            }
            else {
                outputer.showLine("Невозможно выдать сдачу. Не хватает монет :(");
                outputer.write2log("Сдача " + (keepersum-drink.getPrice()) + " р. не выдана. Недостаточно монет в хранилище.");
            }
        }
        return true;
    }

    public boolean finish() { // эмуляция завершения обслуживания
        if(currentState!=states.COOK) return false;
        outputer.showLine("Возьмите напиток и не забудьте сдачу!");
        outputer.write2log("Напиток " + drink.getDrink() + " приготовлен");
        drink.delDrink();
        currentState = states.WAIT;
        return true;
    }

    public void odd(boolean onlylast) { // выдача сдачи
        if(onlylast) { // если true - выдача монеты в сдачу (если монета неверного номинала
            outputer.write2log("Выдача в лоток сдачи последней монеты");
        }
        else { // выдача всех монет и сброс стека с монетками
            outputer.write2log("Выдача в лоток всех монет: " + moneykeeper.getKeeperSum() + " р.");
            moneykeeper.clearKeeper();
            outputer.showLine("Возьмите сдачу!");
        }
    }

    public void clearChange() {
        int k = moneykeeper.getKeeperSum();
        moneykeeper.clearKeeper();
        outputer.write2log("Клиент забрал сдачу " + k + "р.");
        outputer.clearOutput();
    }

    public long getlastChange() {
        return moneykeeper.lastChange;
    }

    public String getCash() {return moneykeeper.getKeeperCoins();}

    public int getCashSum() {return moneykeeper.getKeeperSum();}

    public int[] getCounters() {return moneykeeper.getCounters();}

    public long getCountersSum() {return moneykeeper.getCountersSum();}

    // внутренние переменные
    private states currentState; //текущее состояние
    private String menufile; // файл с содержимым меню
    private List<MenuItem> menulist = new ArrayList<MenuItem>(); // список напитков
    private final int menulinecount = 5; // сколько слов в строке меню при загрузке из файла

    //--------------------------------------------
    static {
    }

    public CoffeeAutomat(String menufile, AbstractCoffeeOutputer outputer) throws CoffeeException{
        this.currentState = states.OFF;
        this.menufile = menufile;
        this.outputer = outputer;
    }

    private void loadMenu() throws CoffeeException { //загрузить меню из файла
        try {
            InputStream in = getClass().getResourceAsStream("/views/menu.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(in,"UTF-8"));
            int i = 0; //счетчик прочитанных строк - для обработчика ошибок
            String line = br.readLine(); // считаем сначала первую строку
            while (line != null) {
                if(!line.substring(0,2).equals("//")) { // отсекаем строчки с комментариями
                    String[] lines = line.split(","); // разделим строку на группы
                    if(lines.length>=menulinecount) { // проверим, что хватает слов в строчке
                        try {
                            menulist.add(new MenuItem(Integer.parseInt(lines[0]), Integer.parseInt(lines[1]) == 0, lines[2], Integer.parseInt(lines[3]), Integer.parseInt(lines[4])));
                        }
                        catch(NumberFormatException ex) { //если ошибка конвертации слова в строке
                            states STATE = currentState;
                            currentState = states.ERROR;
                            throw new CoffeeException(STATE.toString(), i, "Ошибка в файле меню, строка " + i + ", неправильный формат слова, строка: [" + line + "]");
                        }
                    }
                    else {
                        states STATE = currentState;
                        currentState = states.ERROR;
                        throw new CoffeeException(STATE.toString(), lines.length, "Ошибка в файле меню, строка " + i + ", неверное количество параметров (!="+menulinecount+")");
                    }
                }
                line = br.readLine(); // считываем остальные строки в цикле
                i++;
            }
        }
        catch (IOException ex) { // если файл не в папке проекта
            states STATE = currentState;
            STATE = states.ERROR;
            throw new CoffeeException(STATE.toString(), 0, ex.getMessage());
        }
    }

    private int getTypeCount(boolean type) { // возвращает количество наименований с type в меню
        int res=0;
        for(int i=0;i<menulist.size();i++)
            if(menulist.get(i).type==type) res++;
        return res;
    }

    public MenuItem[] getDrinksMenu() { // вернуть меню напитков
        MenuItem[] res = new MenuItem[this.getTypeCount(true)];
        int k=0;
        for(int i=0;i<menulist.size();i++) {
            if(menulist.get(i).type) res[k++]=menulist.get(i);
        }
        return res;
    }

    public MenuItem[] getAddsMenu() { // вернуть меню добавок
        MenuItem[] res = new MenuItem[this.getTypeCount(false)];
        int k=0;
        for(int i=0;i<menulist.size();i++) {
            if(!menulist.get(i).type) res[k++]=menulist.get(i);
        }
        return res;
    }

    public void printDrinksMenu() { // вывести в outputer меню напитков
        MenuItem[] dm = getDrinksMenu();
        for(int i=0;i<dm.length;i++) outputer.showLine("Напиток: "+dm[i].name+", цена: "+dm[i].price);
    }

    public void printAddsMenu() { // вывести в outputer меню добавок
        MenuItem[] dm = getAddsMenu();
        for(int i=0;i<dm.length;i++) outputer.showLine("Добавка: "+dm[i].name+", цена: "+dm[i].price);
    }

}

