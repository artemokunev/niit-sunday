package my.except;

public class CoffeeException extends Exception {
    public String errorState;
    public long errorParameter;

    public CoffeeException(String errorState, long errorParameter, String message) {
        super(message);
        this.errorState = errorState;
        this.errorParameter = errorParameter;
    }
}