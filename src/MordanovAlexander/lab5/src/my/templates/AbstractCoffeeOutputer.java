package my.templates;

import my.except.CoffeeException;

public interface AbstractCoffeeOutputer {
    void write2log(String message);

    void showLine(String message);

    void stop() throws CoffeeException;

    void clearOutput();
}