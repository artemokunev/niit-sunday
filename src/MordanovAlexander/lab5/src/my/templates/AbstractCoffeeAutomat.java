package my.templates;

public interface AbstractCoffeeAutomat {

    boolean on(); // эмуляция включения автомата

    boolean off(); // эмуляция выключения автомата

    boolean coin(int coin); // эмуляция внесения денег (проверка на правильность)

    boolean choice(int i); // эмуляция выбора напитка + проверка заказа (на добавки)

    boolean check(); // эмуляция проверки заказа (стоимость)

    boolean cancel(); // эмуляция отмены заказа

    boolean cook(); // эмуляция приготовления заказа

    boolean finish(); // эмуляция завершения обслуживания

    enum states {
        OFF, WAIT, ACCEPT, CHECK, COOK, ERROR //состояние автомата
    }

}
