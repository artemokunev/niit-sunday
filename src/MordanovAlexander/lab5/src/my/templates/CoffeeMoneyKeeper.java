package my.templates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CoffeeMoneyKeeper {
    public Integer[] validcoins = new Integer[] {
            1,2,5,10,50 //возможные монетки/купюры
    };
    private int[] coinCounters = new int[validcoins.length]; // счетчики количества монет
    public long lastChange = 0; // сдача после каждого flush

    public List<Integer> currentCoins = new ArrayList<Integer>() {}; // текущий счетчик монет (хранит не сумму, а массив вложенных в приемник монет)

    public CoffeeMoneyKeeper() {
        Arrays.sort(validcoins);
        Arrays.fill(coinCounters, 0); // заполним нулями
    }

    public void clearKeeper() { // очистить сборщик монеток
        this.currentCoins.clear();
    }

    public boolean checkCoin(int coin) { // функция проверки валидности монетки
        Integer k = Arrays.asList(validcoins).indexOf(coin);
        return k>=0;
    }

    public long getCountersSum() { // сумма монеток в хранилище
        long sum=0;
        for(int i=0;i<coinCounters.length;i++)
            sum += coinCounters[i]*validcoins[i];
        return sum;
    }

    public int getKeeperSum() { // сумма монеток в сборщике
        return currentCoins.stream().mapToInt(Integer::intValue).sum();
    }

    public String getKeeperCoins() {return currentCoins.toString();}

    public int getKeeperCount() { // количество монеток в сборщике
        return currentCoins.size();
    }

    public int[] getCounters() { // получить массив с количество монет в хранилище
        return coinCounters;
    }

    public int insertCoin(int coin) { // кидание одной монетки, возвращает количество монеток в сборщике, в случае ошибки возвращает -1
        if(checkCoin(coin)) {
            currentCoins.add(coin);
            return getKeeperCount();
        }
        else return -1;
    }

    public boolean odd(long amount) { // выдача сдачи. выдается начиная с крупных монет, когда заканчиваются - с более мелких. если нет сдачи или нет нужного номинала монет - вовращает false
        int k = validcoins.length - 1;
        this.lastChange = amount;
        while(k>=0) {
            if((coinCounters[k] == 0) || (validcoins[k] > amount)) k--; // если в счетчике пусто или сдача меньше номинала счетчика
            else {
                int coinInNeed = (int)(amount - (amount % validcoins[k])) / validcoins[k]; // подсчитаем сколько монеток надо
                if(coinInNeed>coinCounters[k]) { // если в хранилище меньше, чем в счетчике, вычтем сколько есть и пойдем дальше
                    coinInNeed = coinCounters[k];
                }
                amount -= coinInNeed*validcoins[k];
                coinCounters[k] -= coinInNeed;
            }
        }
        return amount==0;
    }

    public void flushKeeper() { // очистка сборщика монет и помещение монет в хранилище
        for(int i=0;i<currentCoins.size();i++) {
            int k = currentCoins.get(i);
            int m = Arrays.asList(validcoins).indexOf(k);
           coinCounters[m]++;
        }
        clearKeeper();
    }

}
