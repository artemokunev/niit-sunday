package my.templates;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class CoffeeMoneyKeeperTest {
    @Test
    public void insertCoin() throws Exception {
        CoffeeMoneyKeeper mk = new CoffeeMoneyKeeper();
        mk.insertCoin(50);
        mk.insertCoin(5);
        mk.insertCoin(5);
        mk.insertCoin(5);
        mk.insertCoin(5);
        mk.insertCoin(1);
        mk.insertCoin(1);
        mk.insertCoin(2);
        mk.flushKeeper();
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(1)]==2);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(2)]==1);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(5)]==4);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(10)]==0);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(50)]==1);
    }

    @Test
    public void odd0() throws Exception { // проверка правильной выдачи сдачи
        CoffeeMoneyKeeper mk = new CoffeeMoneyKeeper();
        mk.insertCoin(10);
        mk.insertCoin(5);
        mk.insertCoin(2);
        mk.flushKeeper();
        assertTrue(mk.odd(17));
        for(int i:mk.getCounters()) {
            assertTrue(mk.getCounters()[i] == 0);
        }

    }

    @Test
    public void odd00() throws Exception { // проверка правильной выдачи сдачи
        CoffeeMoneyKeeper mk = new CoffeeMoneyKeeper();
        mk.insertCoin(50);
        mk.insertCoin(50);
        mk.insertCoin(10);
        mk.insertCoin(10);
        mk.insertCoin(10);
        mk.insertCoin(5);
        mk.insertCoin(5);
        mk.insertCoin(5);
        mk.insertCoin(5);
        mk.insertCoin(2);
        mk.insertCoin(2);
        mk.insertCoin(2);
        mk.insertCoin(2);
        mk.insertCoin(2);
        mk.insertCoin(1);
        mk.insertCoin(1);
        mk.flushKeeper();
        assertTrue(mk.odd(94));
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(1)] == 2);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(2)] == 3);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(5)] == 2);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(10)] == 0);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(50)] == 1);
        assertTrue(mk.getCountersSum()==68);

    }

    @Test
    public void odd1() throws Exception { // проверка правильности разнесения по хранилищам
        CoffeeMoneyKeeper mk = new CoffeeMoneyKeeper();
        mk.insertCoin(10);
        mk.insertCoin(5);
        mk.insertCoin(1);
        mk.insertCoin(2);
        mk.insertCoin(2);
        mk.flushKeeper();
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(1)] == 1);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(2)] == 2);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(5)] == 1);
        assertTrue(mk.getCounters()[Arrays.asList(mk.validcoins).indexOf(10)] == 1);

    }

    @Test
    public void odd2() throws Exception { //требуется сдать 20, есть только 17
        CoffeeMoneyKeeper mk = new CoffeeMoneyKeeper();
        mk.insertCoin(10);
        mk.insertCoin(5);
        mk.insertCoin(2);
        mk.flushKeeper();
        assertFalse(mk.odd(20));
    }

}