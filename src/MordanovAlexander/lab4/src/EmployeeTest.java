import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeTest {
    private final static long p1val = 100000;
    private final static long p2val = 1000;
    private final static long p3val = 5000;
    private final static long p4val = 15000;
    private final static long p5val = 4000;

    private final static double eng_prj_rate = 0.1;
    private final static double mng_prj_rate = 0.15;

    @Test
    public void test0() {
        Employee e = new Driver(1, "Тест", 10, 400, "Driver");
        assertTrue(e.getEmployeePayment()==10*400);
    }

    @Test
    public void test1() {
        Employee e = new Cleaner(1, "Тест", 10, 400, "Cleaner");
        assertTrue(e.getEmployeePayment()==10*400);
    }

    @Test
    public void test2() { // тестер получает базовую зарплату плюс 10% за каждый проект
        Tester e = new Tester(1, "Тест", 10, 400, "Tester");
        e.addProject("project0", p1val);
        e.addProject("project1", p2val);
        assertTrue(e.getEmployeePayment()== (10*400 + p1val*eng_prj_rate + p2val*eng_prj_rate));
    }

    @Test
    public void test3() { // программер получает базовую зарплату плюс 10% за каждый проект
        Programmer e = new Programmer(1, "Тест", 10, 400, "Programmer");
        e.addProject("project0", p1val);
        e.addProject("project1", p3val);
        assertTrue(e.getEmployeePayment()== (10*400 + p1val*eng_prj_rate + p3val*eng_prj_rate));
    }

    @Test
    public void test4() { // тимлид получает базовую зарплату плюс 10% за каждый проект плюс базовый оклад за каждый проект
        TeamLeader e = new TeamLeader(1, "Тест", 10, 400, "Programmer");
        e.addProject("project0", p1val);
        e.addProject("project1", p3val);
        assertTrue(e.getEmployeePayment()== (10*400 + p1val*eng_prj_rate + p3val*eng_prj_rate + 2*400));
    }

    @Test
    public void test5() { // менеджер получает 15% за каждый проект (да, больше ничего)
        Manager e = new Manager(1, "Тест", "Manager");
        e.addProject("project0", p4val);
        e.addProject("project1", p5val);
        assertTrue(e.getEmployeePayment()== (p4val*mng_prj_rate + p5val*mng_prj_rate));
    }

    @Test
    public void test6() { // проджектменеджер получает 15% за каждый проект плюс 1000 за каждый проект
        ProjectManager e = new ProjectManager(1, "Тест", "ProjectManager");
        e.addProject("project0", p1val);
        e.addProject("project1", p2val);
        e.addProject("project2", p4val);
        e.addProject("project3", p5val);
        assertTrue(e.getEmployeePayment()== (p1val*mng_prj_rate + p2val*mng_prj_rate + p4val*mng_prj_rate + p5val*mng_prj_rate + 4*1000));
    }

    @Test
    public void test7() { // главный менеджер получает 15% за каждый проект плюс 1000 за каждый проект плюс бонус 50000
        SeniorManager e = new SeniorManager(1, "Тест", "SeniorManager");
        e.addProject("project0", p1val);
        e.addProject("project1", p2val);
        e.addProject("project2", p3val);
        e.addProject("project3", p4val);
        e.addProject("project4", p5val);
        assertTrue(e.getEmployeePayment()== (p1val*mng_prj_rate + p2val*mng_prj_rate + p3val*mng_prj_rate + p4val*mng_prj_rate + p5val*mng_prj_rate + 5*1000 + 50000));
    }

}