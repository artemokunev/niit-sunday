import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class StaffDemo {

    public static void main(String[] args) {
        StaffClass staff; // общий класс - описан ниже, загружает из файлов и хранит коллекции

        if(args.length<2) {
            System.out.println("Параметры: {1} - имя файла проектов, {2} - имя файл работников");
            return;
        }

        staff = new StaffClass(args[0], args[1]);
        System.out.println("Загружаем файл с проектами: " + args[0]);
        staff.loadProjects();                                                 // загружаем проекты из файла
        System.out.println("Проектов: " + staff.getProjects().size());
        System.out.println("Загружаем файл с работниками: " + args[1]);
        try{
            staff.loadWorkers();                                             // загружаем работников из файла
        }
        catch(Exception ex) {
            System.out.println("Ошибка загрузки файла работников: " + ex.getMessage());
            return;
        }
        System.out.println("Работников: " + staff.getWorkers().size());

        System.out.println("-------------------------------------------");
        System.out.println("Зарплата работников");
        for(Employee e:staff.getWorkers()) {                                 // выводим список того, что каждый заработал
            NumberFormat formatter = new DecimalFormat("#0.00");
            System.out.println("ID: " + e.getId() + ", ФИО: " + e.getFIO() + " (" + e.workname + "): " + formatter.format(e.getEmployeePayment()) + "р.");
        }

    }

}

//--------------------------ГЛАВНЫЙ КЛАСС-------------------------------
class StaffClass {                                           // общий класс для всего персонала
    private String file_projects;
    private String file_workers;

    private List<Employee> empl = new ArrayList<Employee>() {};
    private List<ProjectDescription> prj = new ArrayList<ProjectDescription>() {};

    StaffClass(String file_projects, String file_workers) {
        this.file_projects = file_projects;
        this.file_workers = file_workers;
    }

    List<ProjectDescription> getProjects() {
        return prj;
    }

    List<Employee> getWorkers() {
        return empl;
    }

    void loadProjects() { // загрузить список проектов
        File fg = new File(file_projects);
        try {
            FileReader fr = new FileReader(fg);
            JSONParser parser = new JSONParser();
            JSONObject js = (JSONObject)parser.parse(fr);
            JSONArray items = (JSONArray)js.get("project");
            for(Object i:items) {
                JSONObject k = (JSONObject)i;
                System.out.println(k.get("name").toString());
                long b = Long.parseLong(k.get("budget").toString());
                prj.add(new ProjectDescription(k.get("name").toString(), b)); // добавим в массив
            }
        }
        catch(FileNotFoundException ex) {
            System.out.println("Файл с проектами не найден! (" + file_projects + "), ошибка: " + ex.getMessage());
        }
        catch(IOException ex) {
            System.out.println("Ошибка чтения из файла проектов " + file_projects +": " + ex.getMessage());
        }
        catch(ParseException ex) {
            System.out.println("Ошибка парсера: " + ex.getMessage());
        }
    }

    void loadWorkers() throws Exception { // загрузить список работников
        File fg = new File(file_workers);
        try {
            FileReader fr = new FileReader(fg);
            JSONParser parser = new JSONParser();
            JSONObject js = (JSONObject)parser.parse(fr);
            JSONArray items = (JSONArray)js.get("worker");
            for(Object i:items) {
                JSONObject k = (JSONObject)i;
                String pos = k.get("position").toString();
                int id = Integer.parseInt(k.get("id").toString());
                String name = k.get("name").toString();
                long base = 0;
                try {
                    base = Long.parseLong(k.get("base").toString());
                }
                catch(NullPointerException ex) {
                    // базового оклада может и не быть, например у проджект-менеджера
                }
                int wt = Integer.parseInt(k.get("worktime").toString());
                switch(pos) {
                    case "Cleaner":
                        empl.add(new Cleaner(id, name, wt, base, pos));
                        break;
                    case "Programmer":
                        Programmer p = new Programmer(id, name, wt, base, pos);
                        getProjects(p, k);
                        empl.add(p);
                        break;
                    case "ProjectManager":
                        ProjectManager pm = new ProjectManager(id, name, pos);
                        getProjects(pm, k);
                        empl.add(pm);
                        break;
                    case "SeniorManager":
                        SeniorManager sm = new SeniorManager(id, name, pos);
                        getProjects(sm, k);
                        empl.add(sm);
                        break;
                    case "Tester":
                        Tester t = new Tester(id, name, wt, base, pos);
                        getProjects(t, k);
                        empl.add(t);
                        break;
                    case "TeamLeader":
                        TeamLeader tl = new TeamLeader(id, name, wt, base, pos);
                        getProjects(tl, k);
                        empl.add(tl);
                        break;
                    case "Manager":
                        Manager m = new Manager(id, name, pos);
                        getProjects(m, k);
                        empl.add(m);
                        break;
                    case "Driver":
                        empl.add(new Driver(id, name, wt, base, pos));
                        break;
                    default:
                        throw new Exception("Нет такого класса: " + pos + "!");
                }
            }
        }
        catch(FileNotFoundException ex) {
            System.out.println("Файл с работниками не найден! (" + file_workers + "), ошибка: " + ex.getMessage());
        }
        catch(IOException ex) {
            System.out.println("Ошибка чтения из файла работников " + file_workers +": " + ex.getMessage());
        }
        catch(ParseException ex) {
            System.out.println("Ошибка парсера: " + ex.getMessage());
        }
    }

    private ProjectDescription searchProjectDescription(String pname) {
        for(ProjectDescription pd:prj) {
            if(pd.getName().equals(pname))
                return pd;
        }
        return null;
    }

    private void getProjects(Manager m, JSONObject obj) { // добавить проекты менеджеру
        try {
            JSONArray prjs = (JSONArray)obj.get("projects");
            if(!prjs.isEmpty()) {
                for(Object p0: prjs) {
                    ProjectDescription pd = searchProjectDescription(p0.toString());
                    if (pd != null)
                        m.addProject(pd);
                }
            }
        }
        catch(NullPointerException ex) {
            // ничего не делаем - нет у человека проектов
        }

    }

    private void getProjects(Engineer e, JSONObject obj) { // добавить проекты инженеру
        try {
            JSONArray prjs = (JSONArray)obj.get("projects");
            if(!prjs.isEmpty()) {
                for(Object p0: prjs) {
                    ProjectDescription pd = searchProjectDescription(p0.toString());
                    if (pd != null)
                        e.addProject(pd);
                }
            }
        }
        catch(NullPointerException ex) {
            // ничего не делаем - нет у человека проектов
        }

    }

}

//--------------------------ИНТЕРФЕЙСЫ-------------------------------

interface Heading {
    int defaultheadingbase = 1000; // базовая ставка за руководство если нет своей ставки

    double getHeadingPayment(int emplCount, double base); // количество подчиненных * базовая ставка
}

interface WorkTime {

    void getWorkTimePayment(int workHours, double base); // подсчет оплаты за время работы
}

interface Project {
    double getProjectPayment(long projBudget, double projValue); // оплата за проект
}

//--------------------------АБСТРАКТНЫЕ КЛАССЫ-------------------------------

abstract class Employee {
    private int id;          // табельный номер
    private String fio;      // ФИО
    double salary; // зарплата
    String workname; // название должности

    Employee(int id, String fio) {
        this.id = id;
        this.fio = fio;
    }

    String getFIO() {
        return fio;
    }

    int getId() {
        return id;
    }

    abstract double getEmployeePayment(); // расчет заработной платы
}

//--------------------------ВЕРХНЕУРОВНЕВЫЕ КЛАССЫ-------------------------------

class Personal extends Employee implements WorkTime {
    private double base; // базовая ставка
    private int worktime; // отработанное время

    Personal(int id, String fio, int worktime, double base) {
        super(id, fio);
        this.worktime = worktime;
        this.base = base;
    }

    @Override
    public void getWorkTimePayment(int workHours, double base) {
        salary = workHours * base; // зарплата
    }

    @Override
    double getEmployeePayment() {
        getWorkTimePayment(worktime, base);
        return salary;
    }
}

 class Engineer extends Employee implements WorkTime, Project {
    private double base; // базовая ставка
    private int worktime;
    private List<ProjectDescription> projectdesc = new ArrayList<>();

    private final static double project_rate = 0.1;

    Engineer(int id, String fio, int worktime, double base) {
        super(id, fio);
        this.worktime = worktime;
        this.base = base;
    }

    List<ProjectDescription> getProjects() {
        return projectdesc;
    }

     void addProject(String name, long budget) {
         projectdesc.add(new ProjectDescription(name, budget));
     }

     void addProject(ProjectDescription pd) {
         projectdesc.add(pd);
     }

     public void getWorkTimePayment(int workHours, double base) {
        salary = workHours * base;
        for(ProjectDescription p:projectdesc)
            salary += this.getProjectPayment(p.getBudget(), project_rate); // получает 10% от проекта
    }

    @Override
    public double getEmployeePayment() {
        getWorkTimePayment(worktime, base);
        return salary;
    }

     @Override
     public double getProjectPayment(long projBudget, double projValue) {
         return projBudget*projValue;
     }
 }

class ProjectDescription { // описание проекта
    private String pname; // название
    private long budget; // бюджет

    ProjectDescription(String pname, long budget) {
        this.pname = pname;
        this.budget = budget;
    }

    long getBudget() {
        return budget;
    }

    String getName() {
        return pname;
    }

}

//--------------------------РЕАЛЬНЫЕ КЛАССЫ-------------------------------

class Driver extends Personal implements WorkTime { //водитель

    Driver(int id, String fio, int worktime, double base, String workname) {
        super(id, fio, worktime, base);
        this.workname = workname;
    }

}

class Cleaner extends Personal implements WorkTime { // уборщик

    Cleaner(int id, String fio, int worktime, double base, String workname) {
        super(id, fio, worktime, base);
        this.workname = workname;
    }

}

class Tester extends Engineer { // тестер
    Tester(int id, String fio, int worktime, double base, String workname) {
        super(id, fio, worktime, base);
        this.workname = workname;
    }
}

class Programmer extends Engineer { // программист
    Programmer(int id, String fio, int worktime, double base, String workname) {
        super(id, fio, worktime, base);
        this.workname = workname;
    }
}

class TeamLeader extends Programmer implements Heading { // тимлид
    @Override
    public double getHeadingPayment(int emplCount, double base) {
        return emplCount*base;
    }

    TeamLeader(int id, String fio, int worktime, double base, String workname) {
        super(id, fio, worktime, base, workname);
    }

    @Override
    public void getWorkTimePayment(int workHours, double base) {
        super.getWorkTimePayment(workHours, base);
        salary += getHeadingPayment(getProjects().size(), base); // получает N прибавок за каждый проект, который ведет
    }

}

class Manager extends Employee implements Project { // менеджер
    private List<ProjectDescription> projectdesc = new ArrayList<>();

    private final static double project_rate = 0.15;

    Manager(int id, String fio, String workname) {
        super(id, fio);
        this.workname = workname;
    }

    List<ProjectDescription> getProjects() {
        return projectdesc;
    }

    void addProject(String name, long budget) {
        projectdesc.add(new ProjectDescription(name, budget));
    }

    void addProject(ProjectDescription pd) {
        projectdesc.add(pd);
    }

    private void getPayment() {
        salary=0;
        for(ProjectDescription p:projectdesc)
            salary += this.getProjectPayment(p.getBudget(), project_rate); // получает 15% за ведение проекта
    }

    @Override
    public double getEmployeePayment() {
        getPayment();
        return salary;
    }

    @Override
    public double getProjectPayment(long projBudget, double projValue) {
        return projBudget*projValue;
    }

}

class ProjectManager extends Manager implements Heading { // проджектменеджер
    @Override
    public double getHeadingPayment(int emplCount, double base) {
        return emplCount*base;
    }

    ProjectManager(int id, String fio, String workname) {
        super(id, fio, workname);
    }

    @Override
    public double getEmployeePayment() {
        salary = super.getEmployeePayment()+getHeadingPayment(getProjects().size(), defaultheadingbase);
        return salary;
    }
}

class SeniorManager extends ProjectManager { // главный

    SeniorManager(int id, String fio, String workname) {
        super(id, fio, workname);
    }

    @Override
    public double getEmployeePayment() {
        salary = super.getEmployeePayment() + 50000;
        return salary; // CEO - бонус к зарплате
    }
}