import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Ilya on 02.07.2017.
 */
public class SqrtTest {
    @Test
    public void calc() throws Exception {
        Sqrt sqrttest = new Sqrt();
        assertTrue(sqrttest.calc(64,0.000001)<=8.000001);
    }

    @Test
    public void average() throws Exception {
        assertTrue(Sqrt.average(3,11)==7);
    }

}