import java.util.Arrays;

public class contractor {

    public static void main(String[] args) {
        if(args.length < 1) { //проверяем что есть параметр
            System.out.println("Parameters: [1] - string for contract");
            System.out.println("String format: [N],[N],...");
            return;
        }
        Contract cntr = new Contract(args[0]);
        System.out.println("Result = " + cntr.contractAll());
    }
}

class Contract {
    private String sourcestr; // исходная строка
    private int[] iwords; // массив разбитых слов

    Contract (String srcstr) {
        String[] words;
        int k=0;
        this.sourcestr = srcstr;
        words = sourcestr.split(","); //делим на слова через зпт
        for(int i=0;i<words.length;i++) {
            if(isNumber(words[i])) k++; // посчитаем, сколько слов - числа
        }
        this.iwords = new int[k]; // определим массивчик
        k=0;
        for(int i=0;i<words.length;i++) {
            if(isNumber(words[i])) {
                iwords[k++]=Integer.parseInt(words[i]); // заполним новый массивчик числами
            }
        }
        Arrays.sort(iwords); // отсортируем массив нв всякий случай
    }

    public String contractAll() {
        int sindex=-1; // индекс элемента, начинающего последовательность для сворачивания
        int i=0;

        String res = "";
        while(i<iwords.length) { // посмотрим на все
            if(i==iwords.length-1) { // если смотрим на последний элемент
                if(sindex<0) res+=iwords[i]; // если нет признака, что это последовательность - выводим элемент
                else res+=outconj(iwords[sindex],iwords[i]); // если есть признак, что это последовательность - выводим последовательность
                i++;
            }
            else {
                if (iwords[i] == iwords[i + 1] - 1) {
                    if (sindex < 0) sindex = i;
                    i++;
                } else {
                    if (sindex < 0) {// если нет признака, что это последовательность - выводим элемент
                        res += iwords[i] + ",";
                        i++;
                    } else { // если есть признак, что это последовательность - выводим последовательность
                        res += outconj(iwords[sindex], iwords[i])+",";
                        sindex = -1; // сбрасываем признак
                        i++;
                    }
                }
            }
        }
        return res;
    }

    private String outconj(int start, int end) { // функция для вывода последовательности
        switch(end-start) {
            case 0:
                return Integer.toString(start); // если так случилось, что один элемент - выводим его
            case 1:
                return start + "," + end; // если два рядом - выводим через зпт
            default:
                return start + "-" + end; // если два интервалом - выводим через дефис
        }
    }

    private boolean isNumber(String str) { // функция проверки на число
        if (str == null || str.isEmpty()) return false;
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) return false;
        }
        return true;
    }
}