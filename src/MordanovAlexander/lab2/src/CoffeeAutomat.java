import except.CoffeeException;
import templates.AbstractCoffeeAutomat;
import templates.AbstractCoffeeOutputer;
import templates.CoffeeDrink;
import templates.CoffeeMoneyKeeper;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class MenuItem { // описание одного элемента меню
    int index; // номер по порядку
    boolean type; // тип напитка (true - это напиток, false - добавка)
    String name; // название
    int parameter; // параметр - для напитка можно ли добавлять добавки, для добавки - максимальное количество
    long price; // цена

    public MenuItem(int index, boolean type, String name, int parameter, long price) {
        this.index = index;
        this.type = type;
        this.name = name;
        this.parameter = parameter;
        this.price = price;
    }
}

class CoffeeAutomat implements AbstractCoffeeAutomat {

    private AbstractCoffeeOutputer outputer; // класс для вывода ошибок и результата работы
    private CoffeeMoneyKeeper moneykeeper = new CoffeeMoneyKeeper(); // хранилище денег
    private CoffeeDrink drink = new CoffeeDrink(); // хранилище напитка

    public void on() { // эмуляция включения автомата
        this.currentState = states.WAIT;
        try {
            outputer = new CoffeeOutputer("log.txt");
        }
        catch(CoffeeException ex) {
            this.outputer.write2log("Ошибка создания логирователя! " + ex.errorState + ", " + ex.errorParameter + ", " + ex.getMessage());
        }
        try {
            loadMenu();
            this.outputer.write2log("Автомат запущен, меню загружено, ожидание...");
        }
        catch(CoffeeException ex) {
            this.outputer.write2log("Ошибка загрузки меню! " + ex.errorState + ", " + ex.errorParameter + ", " + ex.getMessage());
        }
    }

    public void off() { // эмуляция выключения автомата
        outputer.showLine("Выключение автомата");
        if(moneykeeper.getKeeperSum()>0) {
            outputer.showLine("В счетчике монет " + moneykeeper.getKeeperSum() + " монет на " + moneykeeper.getKeeperCount() + " р.");
            outputer.write2log("Из счетчика в хранилище: " + moneykeeper.getKeeperSum() + " монет на " + moneykeeper.getKeeperCount() + " р.");
            moneykeeper.flushKeeper();
        }
        this.currentState = states.OFF;
        outputer.write2log("Автомат выключается...");
        for(int i=0;i<moneykeeper.getCounters().length;i++) {
            outputer.write2log("В хранилище №" + i + " (номинал: " + moneykeeper.validcoins[i] + ") - " + moneykeeper.getCounters()[i] + " монет на " + moneykeeper.validcoins[i]*moneykeeper.getCounters()[i]);
        }
        outputer.write2log("Итого в хранилище: " + moneykeeper.getCountersSum() + " р.");
        outputer.write2log("Автомат выключен.");
        try {
            outputer.stop();
        } catch (CoffeeException ex) {
            this.outputer.write2log("Ошибка закрытия логирователя! " + ex.errorState + ", " + ex.errorParameter + ", " + ex.getMessage());
        }
    }

    public void coin(int coin) { // эмуляция внесения денег (проверка на правильность)
        if(currentState!=states.WAIT) return;
        currentState = states.ACCEPT;
        if(moneykeeper.checkCoin(coin)) { // проверяем на валидность монетку
            moneykeeper.insertCoin(coin);
            outputer.showLine("Внесена монета в " + coin + " р. (всего: " + moneykeeper.getKeeperSum() + " р.)");
            outputer.write2log("Монета: " + coin);
        }
        else {
            outputer.write2log("Попала неверная монета: " + coin + " р.");
            outputer.showLine("Неверный номинал монеты!");
        }
        currentState = states.WAIT;
    }

    public void choice(int index) { // эмуляция выбора напитка + проверка заказа (на добавки)
        if(currentState!=states.WAIT) return;
        currentState = states.ACCEPT;
        MenuItem mi = null;
        for(int i=0;i<menulist.size();i++) {
            if(menulist.get(i).index==index)
                mi = menulist.get(i); // получим описание напитка
        }
        if(mi==null) {
            currentState = states.ERROR;
            outputer.write2log("Неизвестный напиток с индексом: " + index);
            return;
        }
        if(mi.type) {
            // это напиток
            drink.setDrink(mi.index, mi.parameter!=0, mi.price, mi.name);
            outputer.write2log("Выбран напиток: " + mi.index + ", " + mi.price);
            outputer.showLine("Напиток " + mi.name + " : " + mi.price + " р.");
        }
        else {
            switch(drink.setAdd(mi.index, 1, mi.price, mi.parameter)) {
                case -1: // нельзя добавлять в напиток ничего
                    outputer.write2log("Попытка добавить " + mi.name + " в " + menulist.get(drink.getDrink()).name);
                    outputer.showLine("В напиток " + drink.name + " нельзя добавить добавки!");
                    break;
                case 1: // нельзя добавить - уже максимальное количество добавок
                    outputer.write2log("Добавка: " + mi.index + ", " + mi.price);
                    outputer.showLine("Нельзя добавить " + mi.name + ", макс. количество - " + mi.parameter);
                    break;
                case 2: // добавили новую добавку
                    outputer.write2log("Добавка: " + mi.index + ", " + mi.price);
                    outputer.showLine("+ " + mi.name + " : " + mi.price + " р., сумма: " + drink.getPrice());
                    break;
                default: // добавка была, добавили еще
                    outputer.write2log("Добавка: " + mi.index + ", " + mi.price + ", всего: " + drink.getAddsCount());
                    outputer.showLine("+ " + mi.name + " : " + mi.price + " р., всего: " + drink.getAddsCount() + ", сумма: " + drink.getPrice());
            }
        }
        currentState = states.WAIT;
    }

    public void check() { // эмуляция проверки заказа (стоимость)
        if(currentState!=states.WAIT) return;
        currentState = states.CHECK;
        outputer.write2log("Проверка заказа на сумму " + drink.getPrice());
        if(moneykeeper.getKeeperSum()>=drink.getPrice()) {
            outputer.showLine("Готовим напиток " + drink.name + "...");
        }
        else {
            outputer.write2log("Попытка купить напиток (" + drink.getPrice() + ") за " + moneykeeper.getKeeperSum() + " р.");
            outputer.showLine("Не хватает средств! Выберите другой напиток или внесите еще монеты.");
            currentState = states.WAIT;
        }
    }

    public void cancel() { // эмуляция отмены заказа
        if(currentState!=states.WAIT) return;
        outputer.showLine("Отмена! Возьмите сдачу " + moneykeeper.getKeeperSum() + " р.");
        odd(false);
    }

    public void cook() { // эмуляция приготовления заказа
        long keepersum = moneykeeper.getKeeperSum();
        if(currentState!=states.CHECK) return;
        currentState = states.COOK;
        moneykeeper.flushKeeper();
        if(keepersum>drink.getPrice()) { // нужно сдавать сдачу
            if(moneykeeper.odd(keepersum-drink.getPrice())) {
                outputer.showLine("Возьмите сдачу: " + (keepersum-drink.getPrice()) + " р.");
                outputer.write2log("Сдача " + (keepersum-drink.getPrice()) + " р.");
            }
            else {
                outputer.showLine("Невозможно выдать сдачу. Не хватает монет :(");
                outputer.write2log("Сдача " + (keepersum-drink.getPrice()) + " р. не выдана. Недостаточно монет в хранилище.");
            }
        }
    }

    public void finish() { // эмуляция завершения обслуживания
        if(currentState!=states.COOK) return;
        outputer.showLine("Возьмите напиток и не забудьте сдачу!");
        outputer.write2log("Напиток " + drink.getDrink() + " приготовлен");
        drink.delDrink();
        currentState = states.WAIT;
    }

    public void odd(boolean onlylast) { // выдача сдачи
        if(onlylast) { // если true - выдача монеты в сдачу (если монета неверного номинала
            outputer.write2log("Выдача в лоток сдачи последней монеты");
        }
        else { // выдача всех монет и сброс стека с монетками
            outputer.write2log("Выдача в лоток всех монет: " + moneykeeper.getKeeperSum() + " р.");
            moneykeeper.clearKeeper();
            outputer.showLine("Возьмите сдачу!");
        }
    }

    // внутренние переменные
    private states currentState; //текущее состояние
    private String menufile; // файл с содержимым меню
    private List<MenuItem> menulist = new ArrayList<MenuItem>(); // список напитков
    final int menulinecount = 5; // сколько слов в строке меню при загрузке из файла

    //--------------------------------------------
    static {
    }

    public CoffeeAutomat(String menufile, AbstractCoffeeOutputer outputer) throws CoffeeException{
        this.currentState = states.OFF;
        this.menufile = menufile;
        this.outputer = outputer;
    }

    private void loadMenu() throws CoffeeException { //загрузить меню из файла
        try (FileReader fr = new FileReader(menufile)) {
            BufferedReader br = new BufferedReader(fr);
            int i = 0; //счетчик прочитанных строк - для обработчика ошибок
            String line = br.readLine(); // считаем сначала первую строку
            while (line != null) {
                if(!line.substring(0,2).equals("//")) { // отсекаем строчки с комментариями
                    String[] lines = line.split(","); // разделим строку на группы
                    if(lines.length>=menulinecount) { // проверим, что хватает слов в строчке
                        try {
                            menulist.add(new MenuItem(Integer.parseInt(lines[0]), Integer.parseInt(lines[1]) == 0, lines[2], Integer.parseInt(lines[3]), Integer.parseInt(lines[4])));
                        }
                        catch(NumberFormatException ex) { //если ошибка конвертации слова в строке
                            states STATE = currentState;
                            currentState = states.ERROR;
                            throw new CoffeeException(STATE.toString(), i, "Ошибка в файле меню, строка " + i + ", неправильный формат слова, строка: [" + line + "]");
                        }
                    }
                    else {
                        states STATE = currentState;
                        currentState = states.ERROR;
                        throw new CoffeeException(STATE.toString(), lines.length, "Ошибка в файле меню, строка " + i + ", неверное количество параметров (!="+menulinecount+")");
                    }
                }
                line = br.readLine(); // считываем остальные строки в цикле
                i++;
            }
        }
        catch (IOException ex) { // если файл не в папке проекта
            states STATE = currentState;
            STATE = states.ERROR;
            throw new CoffeeException(STATE.toString(), 0, ex.getMessage());
        }
    }

    private int getTypeCount(boolean type) { // возвращает количество наименований с type в меню
        int res=0;
        for(int i=0;i<menulist.size();i++)
            if(menulist.get(i).type==type) res++;
        return res;
    }

    public MenuItem[] getDrinksMenu() { // вернуть меню напитков
        MenuItem[] res = new MenuItem[this.getTypeCount(true)];
        int k=0;
        for(int i=0;i<menulist.size();i++) {
            if(menulist.get(i).type) res[k++]=menulist.get(i);
        }
        return res;
    }

    public MenuItem[] getAddsMenu() { // вернуть меню добавок
        MenuItem[] res = new MenuItem[this.getTypeCount(false)];
        int k=0;
        for(int i=0;i<menulist.size();i++) {
            if(!menulist.get(i).type) res[k++]=menulist.get(i);
        }
        return res;
    }

    public void printDrinksMenu() { // вывести в outputer меню напитков
        MenuItem[] dm = getDrinksMenu();
        for(int i=0;i<dm.length;i++) outputer.showLine("Напиток: "+dm[i].name+", цена: "+dm[i].price);
    }

    public void printAddsMenu() { // вывести в outputer меню добавок
        MenuItem[] dm = getAddsMenu();
        for(int i=0;i<dm.length;i++) outputer.showLine("Добавка: "+dm[i].name+", цена: "+dm[i].price);
    }

}

class CoffeeOutputer implements AbstractCoffeeOutputer {

    private FileWriter fr;
    private BufferedWriter br;
    private boolean canLog = true;

    public CoffeeOutputer(String logfile) throws CoffeeException{
        try {
            fr = new FileWriter(logfile);
            br = new BufferedWriter(fr);
        }
        catch(FileNotFoundException ex) {
            canLog = false;
            throw new CoffeeException(AbstractCoffeeAutomat.states.OFF.toString(), 0, ex.getMessage());
        }
        catch(IOException ex) {
            canLog = false;
            throw new CoffeeException(AbstractCoffeeAutomat.states.OFF.toString(), 0, ex.getMessage());
        }
    }

    public void write2log(String message){
        Date curdate = new Date();
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
        //System.out.println(message);
        try {
            if(canLog) {
                br.write(df.format(curdate).toString() + " " + message);
                br.write(System.getProperty("line.separator"));
            }
        }
        catch(IOException ex) {
            canLog = false;
        }
        try {
            br.flush();
        } catch (IOException ex) {
            canLog = false;
            // что-то случилось и в лог писать больше не можем :((
        }
    }

    public void showLine(String message) {
        System.out.println(message);
    }

    public void stop() throws CoffeeException {
        try {
            br.flush();
            br.close();
        }
        catch(IOException ex) {
            throw new CoffeeException(AbstractCoffeeAutomat.states.OFF.toString(), 0, ex.getMessage());
        }
    }
}
