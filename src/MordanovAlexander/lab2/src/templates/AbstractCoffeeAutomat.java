package templates;

public interface AbstractCoffeeAutomat {

    void on(); // эмуляция включения автомата

    void off(); // эмуляция выключения автомата

    void coin(int coin); // эмуляция внесения денег (проверка на правильность)

    void choice(int i); // эмуляция выбора напитка + проверка заказа (на добавки)

    void check(); // эмуляция проверки заказа (стоимость)

    void cancel(); // эмуляция отмены заказа

    void cook(); // эмуляция приготовления заказа

    void finish(); // эмуляция завершения обслуживания

    enum states {
        OFF, WAIT, ACCEPT, CHECK, COOK, ERROR //состояние автомата
    }

}
